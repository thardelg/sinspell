import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import aksharawekshakaya.dictionaries.DictionaryManager;
import aksharawekshakaya.spellchecker.SpellChecker;
import aksharawekshakaya.util.Normalizer;
import aksharawekshakaya.util.StringTokenizer;
import aksharawekshakaya.util.Token;
import aksharawekshakaya.util.WordToken;


public class Test2 {
	public static void main(String args[]) {
		
				double tp=1;
				double fp=1; 
				double fn=1; 
				tp = 273;
				fp = 1;
				fn = 22;
				
				
				String testString = "";
				
				String path2 = ("C:\\Users\\Tharindu\\Desktop\\Agg\\s\\" + 22 + ".txt");
				File f = new File(path2);
				Scanner scanner;
				try {
					scanner = new Scanner(f,"utf-16");
				    while (scanner.hasNextLine()){
				    	  testString += scanner.nextLine();}
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				  
		
				//String testString = " කලාතුරකින් අතීතය ණපමණක් පරෙැඑරැයැරරැමණක් නොව අනාගතතය වුව ද සිහිකරන්නේ";
				DictionaryManager dm = DictionaryManager.getInstance();
				System.out.println("LOADING DICTIONARIES!");
				dm.getLexicon();dm.getPhoneticAbslDictionary();dm.getPhoneticDictionary();dm.getsyBiGramDic();
				dm.getUserDictionary();
				System.out.println("LOADING DONE!");
				SpellChecker spc = new SpellChecker(testString, dm);
				
				
				System.out.println("\nTOKEN SET >> \n");
				Normalizer n = new Normalizer("");
				//String testString2 = "ඔස්ටේ‍්‍රලියාවේ  මුල් පදිංචිකරුවා ";
				StringTokenizer stt = new StringTokenizer(n.normalize(testString), new WordToken(0, "wa"));
				//StringTokenizer s2 = new StringTokenizer(n.normalize(testString), new SentanceToken(0, "wa"));
				List<String> words = new ArrayList<String>();
				while(stt.hasNext()){
					Token t = stt.next();
					if(t.isSinhala())
					System.out.println(t.value());
					words.add(t.value());
				}
				
			
				
				System.out.println("\nERROR SET >> \n");
				spc.spellCheck();
				for(String s2 : spc.getRequest().getErrorSet()){
					System.out.println(s2);
					if(dm.getLexicon().contains(s2)) {
						tp++;
					}
				}
				
				for(String s2 : spc.getRequest().getErrorSet()) {
					if(dm.getLexicon().contains(s2)) {
						fn++;
					}
				}
				
				for(String s2 : spc.getRequest().getErrorSet()) {
					if(words.contains(s2)) {
						words.remove(s2); //get correct
					}
				}
				
				for(String s3 : words){
					if(!dm.getLexicon().contains(s3)) {
						fp++;
					}
					
				}
				
				System.out.println("\nPRECISION : " + getPrecision(tp, fp)*100 );
				System.out.println("RECALL  : " + getRecall(tp, fn)*100);
				
	}
	
	public static double getPrecision(double tp, double fp) {
		return tp/(tp+fp);
		
	}
	
	public static double getRecall(double tp, double fn) {
		return tp/(tp+fn);
		
	}
}
