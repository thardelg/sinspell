import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import aksharawekshakaya.config.Configuration;
import aksharawekshakaya.config.PropertyConfig;
import aksharawekshakaya.dictionaries.FileManager;
import aksharawekshakaya.dictionaries.KeyBasedDictionary;
import aksharawekshakaya.ngram.FileNgramExtractor;
import aksharawekshakaya.ngram.NgramStats;
import aksharawekshakaya.ngram.NgramType;


public class Test3 {
	public static void main(String args[]) { 
		double uniPrep = 0.0;
		double biPrep = 0.0;
		uniPrep = 2745;
		biPrep = 952.3;
		
		PropertyConfig ps = PropertyConfig.getInstance();
		
		FileNgramExtractor syUni = new FileNgramExtractor(new File("C:\\Users\\Tharindu\\Desktop\\List.txt"), NgramType.SYLLABLE_UNI, true);
		System.out.println("CREATING!");
		while(!syUni.isReady());
		System.out.println("DONE!");
		FileNgramExtractor syBi = new FileNgramExtractor(new File("C:\\Users\\Tharindu\\Desktop\\List.txt"), NgramType.SYLLABLE_BI, true);
		System.out.println("CREATING!");
		while(!syUni.isReady());
		System.out.println("DONE!");
		for (Entry<String, Double> s : syUni.getFreqDistribution().entrySet()) {
			System.out.println(s.getKey() + " : " + s.getValue());
		}

		
		//NgramStats
		NgramStats st = new NgramStats(syBi, syUni);
		while(!st.isReady());
		System.out.println("WRITING!");
		
		Map<String, Double> probMap = new HashMap<String, Double>();
		Double ct = ps.getProperty(Configuration.SYLLABLE_UNIGRAM_TOTAL, new Double(0));
		for (Entry<String, Double> entry : st.getDistribution().entrySet()) {
			probMap.put(entry.getKey(), (double)entry.getValue()/ct);
		}
		
		probMap.putAll(st.getDistribution());
		double ascPrep = Math.log(uniPrep);
		double fp = Math.pow(2, ascPrep);
		
		KeyBasedDictionary keyDic = new KeyBasedDictionary(new File(" "),probMap );
		FileManager.writeObject(keyDic, new File("C:\\Users\\Tharindu\\Desktop\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\dic\\syllableTriGrams.dic"));
		for (Entry<String, Double> s : probMap.entrySet()) {
			System.out.println(s.getKey() + " : " + s.getValue());
			}
		
		System.out.println("UNIGRAM PREPLEXITY " + uniPrep );
		System.out.println("BIGRAM PREPLEXITY " + biPrep);
	}
}
