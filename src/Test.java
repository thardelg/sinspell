
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.instrument.Instrumentation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import aksharawekshakaya.config.Configuration;
import aksharawekshakaya.config.PropertyConfig;
import aksharawekshakaya.dictionaries.DictionaryManager;
import aksharawekshakaya.dictionaries.DiskBasedDictionary;
import aksharawekshakaya.dictionaries.FileManager;
import aksharawekshakaya.dictionaries.KeyBasedDictionary;
import aksharawekshakaya.dictionaries.Lexicon;
import aksharawekshakaya.dictionaries.PhoneticDictionary;
import aksharawekshakaya.dictionaries.RandomAccessor;
import aksharawekshakaya.dictionaries.Trie;
import aksharawekshakaya.dictionaries.TrieBasedDictionary;
import aksharawekshakaya.dictionaries.UserDictionary;
import aksharawekshakaya.ngram.FileNgramExtractor;
import aksharawekshakaya.ngram.NgramAggregator;
import aksharawekshakaya.ngram.NgramStats;
import aksharawekshakaya.ngram.NgramTokenizer;
import aksharawekshakaya.ngram.NgramType;
import aksharawekshakaya.ngram.StringNgramTokenizer;
import aksharawekshakaya.spellchecker.SpellChecker;
import aksharawekshakaya.spellengine.DictionaryBasedDetector;
import aksharawekshakaya.spellengine.GenericDetector;
import aksharawekshakaya.spellengine.NgramBasedSuggester;
import aksharawekshakaya.spellengine.PhoneticTransformator;
import aksharawekshakaya.spellengine.RuleBasedDetector;
import aksharawekshakaya.spellengine.SoundexTransformator;
import aksharawekshakaya.spellengine.Suggestion;
import aksharawekshakaya.util.Convertible;
import aksharawekshakaya.util.NormalForm;
import aksharawekshakaya.util.Normalizer;
import aksharawekshakaya.util.SentanceToken;
import aksharawekshakaya.util.StringTokenizer;
import aksharawekshakaya.util.TextUtils;
import aksharawekshakaya.util.Token;
import aksharawekshakaya.util.WordToken;

public class Test {
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, SecurityException, NoSuchFieldException {
		/*Normalizing */
		Normalizer n = new Normalizer("");
		//System.out.println(n.normalize("ක්‍යම"));
		
		PropertyConfig ps = PropertyConfig.getInstance();
//		System.out.println(ps.getProperty(Configuration.DEFAULT_ENCODING, " "));
//		
//		for(String s : edits("සංවිධානයට ")) {
//			System.out.println(s);
//		}
		
		/*******EDIT DISTANCE*******/
//		System.out.println(computeLevenshteinDistance("සංවියැධානයට", "සංධානය"));
		
//		DictionaryManager dm = DictionaryManager.getInstance();
//		UserDictionary ud = dm.getUserDictionary();
//		for(String s : ud.getAll()) {
//			System.out.println(s);
//		}
		//ud.addEntry("අක්ෂරාවේක්ෂකය");

//		
		DictionaryManager dm = DictionaryManager.getInstance();
		System.out.println("LOADING DICTIONARIES!");
		dm.getLexicon();dm.getPhoneticAbslDictionary();dm.getPhoneticDictionary();dm.getsyBiGramDic();
		dm.getUserDictionary();
///		System.out.println("LOADING DONE!");
		//SpellChecker spc = new SpellChecker(" කලාතුරකින් අතීතය ණපමණක් පරෙැඑරැයැරරැමණක් නොව අනාගතතය වුව ද සිහිකරන්නේ", dm);
//		for(Entry<String, HashSet<String>> entry : dm.getPhoneticDictionary().getCodeMap().entrySet()){ 
//		for(String s : entry.getValue()) {
//			System.out.print(s + ", ");
//		}System.out.println();}
	String[] s = {"අංඝනාවෝ", "බැසයණ", "සමානාර්ත", "පඵඩම්", "අපශෝදණය", "ගොටනගන", "දාරීතාව", "දාවනපථය", "කොණ්සල්වරයා", "සමනලයා", "අංඛුර", "ආටම්බරකම"};
		for(String wrd : s) {
			System.out.print(wrd + " -\t");
			Set<String> ss = dm.getPhoneticDictionary().getSimilarSet(wrd);
			for(String g : ss) {
				System.out.print(g + ", ");
		}
			System.out.println();}
	
		//		spc.spellCheck();
//		for(String s : spc.getRequest().getErrorSet()){
//			System.out.println(s);
//		}
		
//		
//		for(Entry<String, Set<Suggestion>> entry : spc.getRequest().getSuggestionsMap().entrySet()) {
//		System.out.print(entry.getKey() + " : ");
//		for(Suggestion s : entry.getValue()) {
//			System.out.print('['+s.getSuggestion()+'|'+s.getScore()+']'  + ", ");
//		}System.out.println();
//	   }
		
//		GenericDetector genDic = new DictionaryBasedDetector(dm.getLexicon());
//		System.out.println(genDic.detect("කලාතුරකින්"));
		
//		for(Entry<String, Set<Suggestion>> entry : spc.getRequest().getSuggestionsMap().entrySet()) {
//		System.out.print(entry.getKey() + " : ");
//		for(Suggestion s : entry.getValue()) {
//			System.out.print(s.getSuggestion() + ", ");
//		}System.out.println();
//	}
		
		//System.out.println(dm.getLexicon().contains("කලාතුරකින්"));
		
//		for(String s : dm.getLexicon().similarSet("කලාතුරකින්", 2)) {
//			System.out.println(s);
//		}
//		for(Entry<String, Set<Suggestion>> entry : spc.getRequest().getSuggestionsMap().entrySet()) {
//			System.out.print(entry.getKey() + " : ");
//			for(Suggestion s : entry.getValue()) {
//				System.out.print(s.getSuggestion() + ", ");
//			}System.out.println();
//		}
		
//		GenericDetector rb = new RuleBasedDetector();
//		System.out.println(rb.getClass().getSimpleName());
		
//		GenericDetector rb = new RuleBasedDetector();
//		System.out.println(rb.detect("ණඅනාගතය"));
		
//		TrieBasedDictionary tb = dm.getLexicon();
////		for(String s : tb.similarSet("එටෙයැ", 2)) {
////			System.out.println(s);
////		}
//		
//		KeyBasedDictionary kb = dm.getsyBiGramDic();
//		KeyBasedDictionary kb2 = dm.getsyTriGramDic();
//		KeyBasedDictionary kb3 = dm.getsyUniGramDic();
//		PhoneticDictionary ph = dm.getPhoneticDictionary();
//		PhoneticDictionary ph2 = dm.getPhoneticAbslDictionary();
//		ph.setSpan(0);
//		for(String s : ph.getSimilarSet("පරිණතතාව")) {
//			System.out.println(s);
//		}
	
		
		//Lexicon lex = new Lexicon(new File("/Aksharawekshakaya/src/Akaradhi.txt"));
		//while(!lex.isReady());
//		File d = new File("resources/dic");
//		for(String s : d.list()) {
//			System.out.println(s);
//		}
//		Lexicon lex2 = new Lexicon(new File("C:\\Users\\Tharindu\\Desktop\\lex\\Cater.txt"));
//		while(!lex2.isReady());
//		Lexicon lex3 = new Lexicon(new File("C:\\Users\\Tharindu\\Desktop\\lex\\Madhura.txt"));
//		while(!lex3.isReady());
//		Set<String> all = new LinkedHashSet<String>();
//		all.addAll(lex.getAll());
//		all.addAll(lex2.getAll());
//		all.addAll(lex3.getAll());

//		BufferedWriter bw = FileManager.getWriter(new File("C:\\Users\\Tharindu\\Desktop\\lex\\All.txt"));
//		StringBuilder sb = new StringBuilder();
//		//Long ct = ps.getProperty(Configuration.WORD_UNIGRAM_TOTAL, new Long(0));
//		for (String s :all) {
//			sb.append(s).append("\n");
//		}
//		try {
//			bw.write(sb.toString());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		KeyBasedDictionary unigram = new KeyBasedDictionary(new File("C:\\Users\\Tharindu\\Desktop\\lex\\List.txt"));
//		while(!unigram.isReady());
//		for(Entry<String, Double> entry : unigram.getCodeMap().entrySet()) {
//			//System.out.println(entry.getKey() + ":" + entry.getValue());
//			//if(entry.getValue()>=1) {
//				all.add(entry.getKey());
//				//System.out.println(entry.getKey() + ":" + entry.getValue());
//			//}
//		}
		
//		BufferedWriter bw2 = FileManager.getWriter(new File("C:\\Users\\Tharindu\\Desktop\\lex\\UnigramOnly.txt"));
//		StringBuilder sb2 = new StringBuilder();
//		//Long ct = ps.getProperty(Configuration.WORD_UNIGRAM_TOTAL, new Long(0));
//		for (String s :all) {
//			sb2.append(s).append("\n");
//		}
//		try {
//			bw2.write(sb2.toString());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
//		PhoneticDictionary phon = new PhoneticDictionary(new File("C:\\Users\\Tharindu\\Desktop\\lex\\All_WithUniGram2.txt"), 3);
		
//		for(Entry<String, HashSet<String>> entry : phon.getCodeMap().entrySet()) {
//			System.out.print(entry.getKey() + " : ");
//			for(String s : entry.getValue()) {
//				System.out.print(s + ", ");
//			}
//			System.out.println();
//		}
//		while(!phon.isReady());
//		FileManager.writeObject(phon, 
//		new File("C:\\Users\\Tharindu\\Desktop\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\dic\\phonetics_new_all_WithUniGram2_3.dic"));
//		
		//read a wordList and perform ternary trie search
//		List<String> wordList = new ArrayList<String>();
//		try{
//			String path = ("C:\\Users\\Tharindu\\Desktop\\Madhura.txt");
//			File f = new File(path);
//			Scanner scanner =  new Scanner(f,"UTF-16");
//			String word = null;
//	        while (scanner.hasNextLine()){
//	        	word = scanner.nextLine().trim();
//		        wordList.add(n.normalize(word));
//		        word = null;
//	        }
//			Trie t = new Trie(wordList);
//			System.out.println("Similar Words >> Trie Size :" + t.size() +">> WordListSize :" + wordList.size());
//			for(String s : t.fuzzySearch(n.normalize("පක්ෂ"), 1)){
//				System.out.println(Normalizer.replaceZJW(s));
//			}
//		} catch(Exception e) {
//			
//		}
		
		//writing file
//		try {
//			BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("C:\\Users\\Tharindu\\Desktop\\Madhura_New.txt"), "UTF-16"));
//			List<String> wordList2 = new ArrayList<String>();
//			for(String s: wordList) {
//				if(s.length()>=3)
//				wordList2.add(s.substring(0, 2) + '\u002A' + s);
//				else{
//					wordList2.add(s+'\u002A' + s);
//				}
//			}
//			Collections.sort(wordList2);
//			for(String s: wordList2) {
//				out.write(s);
//				out.newLine();
//			}
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		
		/*Sinhala set test*/
		//for(char c : TextUtils.sinhalaSet.toCharArray()){
			//System.out.print("'\\u0" + Integer.toHexString((int)c).toUpperCase() +"'" + ", ");
		//}
		//System.out.println(TextUtils.getSinhalaOnly("ggrදහසඑමහැරඑැ dsds අරඇරඇ ටෙයඑරඑරැ යෙ"));
		 
		/*JVM Heap size test
		//Get the jvm heap size.
        //long heapSize = Runtime.getRuntime().totalMemory();
        //Print the jvm heap size.
        //System.out.println("Heap Size = " + (heapSize/1024)/1024);
		
		/*Char in set */
		//System.out.println("\u0E00");
		
		/*Tokenizer*/
//		String testString = "ඔස්ටේ‍්‍රලියාවේ  මුල් පදිංචිකරුවා ";
//		StringTokenizer s = new StringTokenizer(n.normalize(testString), new WordToken(0, "wa"));
//		//StringTokenizer s2 = new StringTokenizer(n.normalize(testString), new SentanceToken(0, "wa"));
//		while(s.hasNext()){
//			Token t = s.next();
//			if(t.isSinhala())
//			System.out.println(t.getStartIndex() +" : "+ t.getEndIndex());
//		}
		
		//RanDic test
//		try {
//			RanDic i = new RanDic(new File("C:\\Users\\Tharindu\\Desktop\\Madhura_New.txt"), "UTF-16");
//			for(String s : i.getWords("අන"))
//			{
//				System.out.println(s);
//			}
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		//DictionarytTest
//		Lexicon lex  = new Lexicon (new File("C:\\Users\\Tharindu\\Desktop\\Madhura.txt"));
//		while(!lex.isReady());
		//System.out.println(lex.contains("කරණම්කාරයෝ"));
//		for(String s : lex.getAll()){
//			System.out.println(s);
//		}
		
		
		//code test
//		List<String> wlist = new ArrayList<String>();
//		for(String s : lex.getAll()){
//			wlist.add(getCode(s));
//		}
//		Collections.sort(wlist);
//		Set<String> ss = new LinkedHashSet<String>(wlist);
//		for(String s2 : ss) {
//			System.out.println(s2);
//		}
//		System.out.println(ss.size());
//	}
	
//	protected static String getCode(final String word) {
//		String code = "";
//		int len = word.length();
//		for(int i=0; i<len; i++) {
//			if((len > 9) && i<8) {
//				code += word.charAt(i);
//			} else if((len > 5) && (len < 10) && (i < 4)) {
//				code += word.charAt(i);
//			} else if ((len > 0) && (len < 6) && (i < 2)) {
//				code += word.charAt(i);
//			} 
//		}
//		return code;
//
//		DiskBasedDictionary dic = new DiskBasedDictionary(new File("C:\\Users\\Tharindu\\Desktop\\List.txt"));
//		while(!dic.isReady());
//		System.out.println("DONE ALL!");
		
//		KeyBasedDictionary dic = new KeyBasedDictionary(new File("C:\\Users\\Tharindu\\Desktop\\List.txt"));
//		while(!dic.isReady());
//		for (String pp : dic.getAll()){
//			System.out.println(pp + " : " + dic.getValue(pp));
//		}
//		System.out.println("DONE ALL!");
		
//		//TESTING RANDOM ACCESS
//		try {
//			RandomAccessor rs = new RandomAccessor(new File("C:\\Users\\Tharindu\\Desktop\\2.txt"));
//			System.out.println(rs.getFreq("මෙsවර"));
//			System.out.println("rerererew");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		//NgramTokenizing
//		NgramTokenizer ng = new StringNgramTokenizer("අකරුණාවන්ත", NgramType.SYLLABLE_TRI);
//		//System.out.println(ng.getContext());
//		for(String s : ng.getTokenSet()) {
//			System.out.println(s);
//		}
		
		//Ngram Aggregator
//		NgramAggregator af = new NgramAggregator(new File("C:\\Users\\Tharindu\\Desktop\\Agg\\s"), NgramType.WORD_BI,false);
//		while(!af.isReady());
//		System.out.println("DONE");
//		for(String s : af.getFreqDistribution().keySet()) {
//			System.out.println(s + " : " + af.getFreqDistribution().get(s));
//		}
		
//		/******SOUNDEX*******/
//		//Transformator
//		PhoneticTransformator pt = new SoundexTransformator(" ", 5);
//		//System.out.println(pt.transform("කොත්කැරැල්ල"));
//		System.out.println(pt.transform("අධ්‍යාපන"));
//		System.out.println(pt.transform("අද්‍යාපන"));
		
		
		//Phonetic Dictionary
//		PhoneticDictionary pd = new PhoneticDictionary(new File("C:\\Users\\Tharindu\\Desktop\\Madhura.txt"), 6);
//		while(!pd.isReady());
//		
//		for(String s : pd.getAll()) {
//			System.out.print(s + " : ");
//			for(String p : pd.getSimilarSet(s))
//					System.out.print(p + ", ");
//			System.out.println();
//		}
		
//		FileNgramExtractor syBi = new FileNgramExtractor(new File("C:\\Users\\Tharindu\\Desktop\\List.txt"), NgramType.SYLLABLE_TRI, true);
//		System.out.println("CREATING!");
//		while(!syBi.isReady());
//		System.out.println("DONE!");
//		for (Entry<String, Double> s : syBi.getFreqDistribution().entrySet()) {
//			System.out.println(s.getKey() + " : " + s.getValue());
//		}
		

		/*******NGRAM STATS*******/
//		FileNgramExtractor syUni = new FileNgramExtractor(new File("C:\\Users\\Tharindu\\Desktop\\List.txt"), NgramType.SYLLABLE_UNI, true);
//		System.out.println("CREATING!");
//		while(!syUni.isReady());
//		System.out.println("DONE!");
//		
//		FileNgramExtractor syBi = new FileNgramExtractor(new File("C:\\Users\\Tharindu\\Desktop\\List.txt"), NgramType.SYLLABLE_BI, true);
//		System.out.println("CREATING!");
//		while(!syUni.isReady());
//		System.out.println("DONE!");
//		for (Entry<String, Double> s : syUni.getFreqDistribution().entrySet()) {
//			System.out.println(s.getKey() + " : " + s.getValue());
//		}
//
//		
//		//NgramStats
//		NgramStats st = new NgramStats(syBi, syUni);
//		while(!st.isReady());
//		System.out.println("WRITING!");
//		
//		Map<String, Double> probMap = new HashMap<String, Double>();
//		Double ct = ps.getProperty(Configuration.SYLLABLE_UNIGRAM_TOTAL, new Double(0));
//		for (Entry<String, Double> entry : st.getDistribution().entrySet()) {
//			probMap.put(entry.getKey(), (double)entry.getValue()/ct);
//		}
//		probMap.putAll(st.getDistribution());
//		
//		
//		KeyBasedDictionary keyDic = new KeyBasedDictionary(new File(" "),probMap );
//		FileManager.writeObject(keyDic, new File("C:\\Users\\Tharindu\\Desktop\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\dic\\syllableTriGrams.dic"));
//		for (Entry<String, Double> s : probMap.entrySet()) {
//			System.out.println(s.getKey() + " : " + s.getValue());
//			}
		/*********************/	
		
		//		BufferedWriter bw = FileManager.getWriter(new File("C:\\Users\\Tharindu\\Desktop\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\ngrams\\wordUniGrams.txt"));
//		StringBuilder sb = new StringBuilder();
//		Long ct = ps.getProperty(Configuration.WORD_UNIGRAM_TOTAL, new Long(0));
//		for (Entry<String, Double> entry : syBi.getFreqDistribution().entrySet()) {
//			sb.append(entry.getKey()).append("\t").append((double)entry.getValue()/(double)ct).append("\n");
//		}
//		try {
//			bw.write(sb.toString());
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		//System.out.println(NgramStats.getGramProb(456, NgramType.SYLLABLE_BI)*1000);
//		
//		UserDictionary ud = new UserDictionary(new File("C:\\Users\\Tharindu\\Desktop\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\dic\\user.txt"));
//		while(!ud.isReady());
//		FileManager.writeObject(ud, new File("C:\\Users\\Tharindu\\Desktop\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\dic\\user.dic"));
		
		//System.out.println(NgramBasedSuggester.getScore("නිශ්ශංක  ", "නිරුදක"));
		
	}
	
//	public static ArrayList<String> edits(String word) {
//		ArrayList<String> result = new ArrayList<String>();
//		for(int i=0; i < word.length(); ++i) result.add(word.substring(0, i) + word.substring(i+1));
//		for(int i=0; i < word.length()-1; ++i) result.add(word.substring(0, i) + word.substring(i+1, i+2) + word.substring(i, i+1) + word.substring(i+2));
//		for(int i=0; i < word.length(); ++i) for(char c='\u0D82'; c <= '\u0DDF'; ++c) result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i+1));
//		for(int i=0; i <= word.length(); ++i) for(char c='\u0D82'; c <= '\u0DDF'; ++c) result.add(word.substring(0, i) + String.valueOf(c) + word.substring(i));
//		return result;
//	}
	
	public static int computeLevenshteinDistance(CharSequence str1,
            CharSequence str2) {
    int[][] distance = new int[str1.length() + 1][str2.length() + 1];

    for (int i = 0; i <= str1.length(); i++)
            distance[i][0] = i;
    for (int j = 1; j <= str2.length(); j++)
            distance[0][j] = j;

    for (int i = 1; i <= str1.length(); i++)
            for (int j = 1; j <= str2.length(); j++)
                    distance[i][j] = minimum(
                                    distance[i - 1][j] + 1,
                                    distance[i][j - 1] + 1,
                                    distance[i - 1][j - 1]
                                                    + ((str1.charAt(i - 1) == str2.charAt(j - 1)) ? 0
                                                                    : 1));

    return distance[str1.length()][str2.length()];
	}
	 private static int minimum(int a, int b, int c) {
         return Math.min(Math.min(a, b), c);
	 }
}
