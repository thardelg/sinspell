package aksharawekshakaya.dictionaries;

import java.io.File;

import aksharawekshakaya.config.Configuration;
import aksharawekshakaya.config.PropertyConfig;

public class DictionaryManager {
	
	private static DictionaryManager instance;
	//private static String DATA_FILE_PATH;
	//private static String INDEX_FILE_PATH;
	private static String DIC_PATH;
	//private static String NGRAM_PATH;
	protected static PropertyConfig config;
	protected static KeyBasedDictionary syBiGramDic;
	protected static KeyBasedDictionary syTriGramDic;
	protected static KeyBasedDictionary syUniGramDic;
	protected static KeyBasedDictionary wordUniGramDic;
	protected static TrieBasedDictionary lexicon;
	protected static UserDictionary userDic;
	protected static PhoneticDictionary phoneticDic;
	protected static PhoneticDictionary phoneticDicAbsl;
	
	private DictionaryManager() {
		config = PropertyConfig.getInstance();
		//DATA_FILE_PATH = config.getProperty(Configuration.DATA_PATH, " ");
		//INDEX_FILE_PATH = config.getProperty(Configuration.INDEX_PATH, " ");
		DIC_PATH = config.getProperty(Configuration.DICTIONARY_PATH, " ");
		//NGRAM_PATH = config.getProperty(Configuration.NGRAM_PATH, " ");
	}
	
	public static DictionaryManager getInstance() {
		if(instance == null) {
			instance = new DictionaryManager();
		} 
		return instance;
	}
	
	public PhoneticDictionary getPhoneticDictionary() {
		if(phoneticDic == null) {
			phoneticDic =(PhoneticDictionary)FileManager.readObject(new File(DIC_PATH + config.getProperty(Configuration.PHONETIC_DIC, " ")));	
		}
		return phoneticDic;
	}
	
	public PhoneticDictionary getPhoneticAbslDictionary() {
		if(phoneticDicAbsl == null) {
			phoneticDicAbsl =(PhoneticDictionary)FileManager.readObject(new File(DIC_PATH + config.getProperty(Configuration.PHONETIC_DIC_ABSL, " ")));	
		}
		return phoneticDicAbsl;
	}
	
	public  KeyBasedDictionary getsyUniGramDic() {
		if(syUniGramDic == null) {
			KeyBasedDictionary fne  =(KeyBasedDictionary)FileManager.readObject(new File(DIC_PATH + config.getProperty(Configuration.SYLLABLE_UNIGRAM_DIC, " ")));
			syUniGramDic = fne;
		}
		return syUniGramDic;
	}
	
	public  KeyBasedDictionary getsyBiGramDic() {
		if(syBiGramDic == null) {
			KeyBasedDictionary fne  =(KeyBasedDictionary)FileManager.readObject(new File(DIC_PATH + config.getProperty(Configuration.SYLLABLE_BIGRAM_DIC, " ")));
			syBiGramDic = fne;
		}
		return syBiGramDic;
	}
	
	public  KeyBasedDictionary getsyTriGramDic() {
		if(syTriGramDic == null) {
			KeyBasedDictionary fne  =(KeyBasedDictionary)FileManager.readObject(new File(DIC_PATH + config.getProperty(Configuration.SYLLABLE_TRIGRAM_DIC, " ")));
			syTriGramDic = fne;
		}
		return syTriGramDic;
	}
	
	public  KeyBasedDictionary getwordUniGramDic() {
		if(wordUniGramDic == null) {
			KeyBasedDictionary fne  =(KeyBasedDictionary)FileManager.readObject(new File(DIC_PATH + config.getProperty(Configuration.WORD_UNIGRAM_DIC, " ")));
			wordUniGramDic = fne;
		}
		return wordUniGramDic;
	}
	
	public TrieBasedDictionary getLexicon() {
		if(lexicon == null) {
			lexicon = (TrieBasedDictionary)FileManager.readObject(new File(DIC_PATH + config.getProperty(Configuration.LEXICON_DIC, " ")));	
		}
		return lexicon;
	}
	
	public UserDictionary getUserDictionary() {
		if(userDic == null) {
			userDic = (UserDictionary)FileManager.readObject(new File(DIC_PATH + config.getProperty(Configuration.USER_DIC, " ")));	
		}
		return userDic;
	}
	
	public UserDictionary reloadUserDictionary() {
		userDic = (UserDictionary)FileManager.readObject(new File(DIC_PATH + config.getProperty(Configuration.USER_DIC, " ")));	
		return userDic;
	}
	
	public static void writeSerializedDictionary(Object obj, File f) {
		FileManager.writeObject(obj, f);
	}
	
	
	
}
