package aksharawekshakaya.dictionaries;

import java.io.File;
import java.util.Map;
import aksharawekshakaya.util.Normalizer;

public class IndexAccessor {
	
	private String  indexFilePath;
	private String  dataFilesPath;
	private Map<String, Double> index; 
	private boolean isReady;
	
	public IndexAccessor(String indexFilePath, String dataFilesPath) {
		this.indexFilePath = indexFilePath;
		this.dataFilesPath = dataFilesPath;
		isReady = false;
		loadIndexFile();
	}

	protected void loadIndexFile() { // build Index file containing word Files
		if (this.index.isEmpty()) {
			KeyBasedDictionary indexSet = new KeyBasedDictionary(new File(
					indexFilePath + "index.idx"));
			index = indexSet.codeMap;
		}
		isReady = true;
	}

	public long getValue(String key) { // returns 0 if not found
		long val = 0;
		String code = getCode(key);
		loadIndexFile();
		String file = String.valueOf(((int)(index.get(code).doubleValue())));
		try {
			RandomAccessor ran = new RandomAccessor(new File(dataFilesPath
					+ file + ".txt"));
			val = ran.getFreq(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return val;
	}
	
	protected String getCode(final String word) {
		Normalizer n = new Normalizer("");
		String code = n.normalize(word);
		if(code.length()<3){
			return word;
		}else {
			return code.substring(0, 3);
		}
		
	}
	
	public boolean isReady() {
		return isReady;
	}
}
