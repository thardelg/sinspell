package aksharawekshakaya.dictionaries;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Trie implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9122634137648240002L;
	protected TrieNode root;
	protected int size; // number of nodes
	protected int maxKeySize;

	public Trie() {
		
	}
	
	public Trie(Map<String, String> dictionary, boolean valuesToLexicon) {
		for (String word : dictionary.keySet()) {
			String value = dictionary.get(word);
			add(word, value);
			if(valuesToLexicon) {
				add(value, value);
			}
		}
	}
	
	public Trie(Collection<String> wordList){
		for(String word : wordList) {
			add(word, word); //add word as the value
		}
	}
	
	//add adds a word and associated value to lexicon
	public Object add(String word, Object value) {
		Object addedValue = null;
		if( word != null) {
			TrieNode wordNode = find(root, word, 0);
			if (wordNode != null) {
				addedValue = wordNode.getValue();
				wordNode.setValue(value);
			} else {
				root = insert(root, word, 0, value);
			}
		}
		return addedValue;
	}
	
	//get associated value of a word
	public Object getNodeValue(String word) {
		Object val = null;
		if(word != null) {
			TrieNode wordNode = find(root, word, 0);
			if(wordNode != null) {
				val = wordNode.getValue();
			}
		}
		return val;
	}
	// index index of first character in word //recursively add node
	public TrieNode insert(TrieNode node, String word, int charIndex, Object value) { // return required by recursion
		if (charIndex < word.length()) {
			char c = word.charAt(charIndex);
			if (node == null) {
				node = new TrieNode(c);
			}
			char charKey = node.getCharKey();
			if (c < charKey) {
				node.setLeftChild(insert(node.getLeftChild(), word, charIndex,
						value));
			} else if (c == charKey) {
				if (charIndex == word.length() - 1) {
					node.setValue(value);
					node.setWordEnd(true);
					size++;
					maxKeySize = Math.max(maxKeySize, word.length()); //if word length is greater, set it to key size
				}
				node.setMidChild(insert(node.getMidChild(), word, charIndex + 1,
						value));
			} else {
				node.setRightChild(insert(node.getRightChild(), word, charIndex,
						value));
			}
		}
		return node;
	}

	public boolean lookupFrom(TrieNode node, String word, int index) { // index of character
		boolean flag = false;
		if (index < word.length() && node != null) {
			char c = word.charAt(index);
			char charKey = node.getCharKey();
			if (charKey > c) {
				return lookupFrom(node.getLeftChild(), word, index);
			} else if (charKey < c) {
				return lookupFrom(node.getRightChild(), word, index);
			} else {
				if (index == word.length() - 1) {
					if (node.isWordEnd()) {
						flag = true;
					} else {
						return lookupFrom(node.getMidChild(), word, index + 1);
					}
				}
			}
		}
		return flag;
	}
	
	public TrieNode find(TrieNode node, String word, int idx) {
		if (node != null && idx < word.length()) {
			final char c = word.charAt(idx);
			final char charKey = node.getCharKey();
			if (c < charKey) {
				return find(node.getLeftChild(), word, idx);
			} else if (c > charKey) {
				return find(node.getRightChild(), word, idx);
			} else {
				if (idx == word.length() - 1) {
					if (node.isWordEnd()) {
						return node;
					}
				} else {
					return find(node.getMidChild(), word, idx + 1);
				}
			}
		}

		return null;
	}

	public List<String> fuzzySearch(TrieNode node, int dst, //distance
			List<String> matchSet, String match, String word, int idx) { //index
		if ((dst >= 0) && (node != null)) {
			char c;
			if (idx < word.length()) {
				c = word.charAt(idx);
			} else {
				c = (char) (-1); //assign a minimum value
			}
			char charKey = node.getCharKey();
			if ((dst > 0) || (c < charKey)) {
				matchSet = fuzzySearch(node.getLeftChild(), dst, matchSet,
						match, word, idx);
			}
			String newMatch = match + charKey;
			if (c == charKey) {
				if (node.isWordEnd() && (dst >= 0)
						&& ((newMatch.length() + dst) >= word.length())) {
					matchSet.add(newMatch);
				}

				matchSet = fuzzySearch(node.getMidChild(), dst, matchSet,
						newMatch, word, idx + 1);
			} else {
				if (node.isWordEnd()
						&& ((dst - 1) >= 0)
						&& (newMatch.length() + (dst - 1) >= word.length())) {
					matchSet.add(newMatch);
				}
				matchSet = fuzzySearch(node.getMidChild(), dst - 1,
						matchSet, newMatch, word, idx + 1);
			}
			if ((dst > 0) || (c > charKey)) {
				matchSet = fuzzySearch(node.getRightChild(), dst, matchSet,
						match, word, idx);
			}
		}
		return matchSet;
	}
	 
	public List<String> fuzzySearch(String word, int dst) {
		List<String> wordList = new ArrayList<String>();
		return fuzzySearch(root, dst, wordList, "", word, 0 );
	}
	// traversal begins from supplied node
	public List<String> traverse(TrieNode node, String str, List<String> words) {
		if (node != null) {
			words = traverse(node.getLeftChild(), str, words);
			String c = String.valueOf(node.getCharKey());
			if (node.getMidChild() != null) {
				if (node.isWordEnd()) {
					words.add(str + c);
				}
				words = traverse(node.getMidChild(), str + c, words);
			} else {
				words.add(str + c);
			}
			words = traverse(node.getRightChild(), str, words);
		}
		return words;
	}
	
	public int size() { //node count
		return size;
	}
	
	public boolean containsWord(String word) {
		return lookupFrom (root, word, 0);
	}
	
	public Set<String> getLexicon() {
		List<String> words  = new ArrayList<String>();
		Set<String> lexicon = new HashSet<String>();
		lexicon.addAll(traverse(root, "", words));
		return lexicon;
	}

}
