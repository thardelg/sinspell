package aksharawekshakaya.dictionaries;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import aksharawekshakaya.util.TextUtils;

public class RandomAccessor {

	RandomAccessFile ranFile;
	private int span = 200;
	private final String encoding = "UTF-16";
	private final char EOL = '\n';

	public RandomAccessor(File file) throws FileNotFoundException {
		file = FileManager.getFile(file);
		ranFile = new RandomAccessFile(file, "r"); // opening for read only
	}

	public void setSpan(final int span) {
		this.span = span;
	}

	public int getSpan() {
		return span;
	}

	public void close() throws IOException {
		ranFile.close();
	}

	public long getFreq(final String aSymbol) throws IOException {

		long start = 0;
		long pos = ranFile.length();
		long end = ranFile.length();

		while (end > (start + 1)) {
			pos = start + ((end - start) / 2);

			final String[] ngram = read(pos);

			// FIXME this seems to shortcut a bit too early if we haven't found
			// the right position yet
			if (ngram == null) {
				return 0;
			}

			final int c = aSymbol.compareTo(ngram[0]);

			if (c == 0) {
				return Long.parseLong(TextUtils.getDigits(ngram[1]));
			} else if (c > 0) {
				start = pos;
			} else {
				end = pos;
			}

		}

		return 0;
	}

	public String[] read(final long pos) throws IOException // NOPMD
	{
		// span a window around the approximated position
		long start = pos - span;
		if (start < 0) {
			start = 0;
		}
		long end = pos + span;
		if (end > ranFile.length()) {
			end = ranFile.length();
		}

		final int len = (int) (end - start);
		final int newPos = (int) (pos - start);

		ranFile.seek(start);
		final byte[] window = new byte[len];

		ranFile.read(window);

		int i = newPos;

		// search for the beginning and end of the file

		// Go back to the beginning of the line
		while ((i >= 0) && ((char) window[i]) != EOL) {
			i--;
		}

		// remember line start position
		final int newStart = i + 1;

		i = newPos + 1;

		// go to end of line
		while ((i < window.length) && ((char) window[i]) != EOL) {
			i++;
		}

		// remember line end position
		final int newEnd = i;

		// copy the bytes for the current line to a new byte[]
		final byte[] curLine = new byte[newEnd - newStart];
		int index = 0;
		for (int j = newStart; j < newEnd; j++) {
			curLine[index++] = window[j];
		}

		// convert the curLine-byte[] to String
		final String lineAsString = new String(curLine, encoding);

		if (lineAsString.length() == 0) {
			return null;
		}

		String[] ngram = new String[2];
		final int tabOffset = lineAsString.indexOf('\t');
		if (tabOffset != -1) {
			ngram[0] = lineAsString.substring(0, tabOffset);
			ngram[1] = lineAsString.substring(tabOffset + 1,
					lineAsString.length() - 1);
			return ngram;
		}

		return null;
	}
}
