package aksharawekshakaya.dictionaries;

import java.io.Serializable;

public class TrieNode implements Comparable<TrieNode>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -602609097537168845L;
	protected char charKey; // char value
	protected Object value; // value stored in node if any
	protected TrieNode midChild;
	protected TrieNode leftChild;
	protected TrieNode rightChild;
	protected boolean isWordEnd;

	public TrieNode() {
		// empty node
	}

	public TrieNode(char charKey) {
		this.charKey = charKey;
	}

	public TrieNode(char charKey, TrieNode midChild, TrieNode leftChild,
			TrieNode rightChild, Object value, boolean isWordEnd) {
		this.charKey = charKey;
		this.leftChild = leftChild;
		this.midChild = leftChild;
		this.rightChild = leftChild;
		this.value = value;
		this.isWordEnd = isWordEnd;
	}

	@Override
	public int compareTo(TrieNode o) { //required for collation sequencing
		int flag;
		char key = o.getCharKey();
		if(key == charKey) {
			flag = 0;
		} else if (key < charKey) {
			flag = 1;
		} else {
			flag = -1;
		}
		return flag;
	}

	public char getCharKey() {
		return charKey;
	}

	public void setCharKey(char charKey) {
		this.charKey = charKey;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public TrieNode getMidChild() {
		return midChild;
	}

	public void setMidChild(TrieNode midChild) {
		this.midChild = midChild;
	}

	public TrieNode getLeftChild() {
		return leftChild;
	}

	public void setLeftChild(TrieNode leftChild) {
		this.leftChild = leftChild;
	}

	public TrieNode getRightChild() {
		return rightChild;
	}

	public void setRightChild(TrieNode rightChild) {
		this.rightChild = rightChild;
	}

	public boolean isWordEnd() {
		return isWordEnd;
	}

	public void setWordEnd(boolean isWordEnd) {
		this.isWordEnd = isWordEnd;
	}

}
