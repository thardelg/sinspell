package aksharawekshakaya.dictionaries;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class TrieBasedDictionary extends Lexicon {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8695776160619685125L;
	protected List<String> words;
	protected Trie wordsTrie;
	private boolean ready;

	public TrieBasedDictionary(File entryFile) {
		super(entryFile);
		ready = false;
		while(!super.isReady());
		words = new ArrayList<String>();
		if(super.isReady()){
			words.addAll(wordList);
			Collections.sort(words);
			wordsTrie = new Trie(words);
			words.clear();
			ready = true;
		}
	}
	
	@Override
	public boolean contains(String word) {
		
		for(String s : wordsTrie.fuzzySearch(word, 2)) {
			if(word.equals(s)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Set<String> getAll() {
		if(ready) 
			return wordsTrie.getLexicon();
		else
			return null;
	}

	@Override
	public int size() {
		if(ready)
			return wordsTrie.size();
		else
			return 0;
	}
	
	public List<String> similarSet(String word, int distance) {
		return wordsTrie.fuzzySearch(word, distance);
	}
	
	@Override
	public boolean isReady() {
		return ready;
	}

}
