package aksharawekshakaya.dictionaries;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import aksharawekshakaya.util.FileTokenizer;
import aksharawekshakaya.util.Token;
import aksharawekshakaya.util.WordToken;

public class Lexicon extends BaseDictionary {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1623907484409647178L;
	private boolean ready;
	protected Set<String> wordList;

	public Lexicon(File entryFile) {
		super(entryFile);
		ready = false;
		wordList = new LinkedHashSet<String>();
		load();
	}

	@Override
	public void load() {
		FileTokenizer tokenizer = new FileTokenizer(entryList, new WordToken(0, " "));
		while(tokenizer.hasNext()){
			Token t = tokenizer.next();
			if(t.isSinhala()) {
				wordList.add(normalizer.normalize(t.value()));
			}
		}
		List<String> words  = new ArrayList<String>(wordList);
		Collections.sort(words);
		wordList.clear();
		wordList.addAll(words);
		ready = true;
	}

	@Override
	public boolean contains(String word) {
		return wordList.contains(word);
	}

	@Override
	public Set<String> getAll() {
		if(ready) 
			return wordList;
		else
			return null;
	}

	@Override
	public int size() {
		if(ready)
			return wordList.size();
		else
			return 0;
	}
	
	@Override
	public boolean isReady() {
		return ready;
	}

}
