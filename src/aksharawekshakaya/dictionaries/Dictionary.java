package aksharawekshakaya.dictionaries;

import java.io.File;
import java.util.Set;

public interface Dictionary {
	public boolean setFile(File entryFile); //entry file can be of different formats for concrete types and of default encoding
	public File getFile();
	public void load();
	public boolean contains(String entry);
	public Set<String> getAll();
	public int size();
	public boolean isReady(); //checks if file is loaded
}
