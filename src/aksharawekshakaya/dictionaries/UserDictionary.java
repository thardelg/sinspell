package aksharawekshakaya.dictionaries;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import aksharawekshakaya.config.Configuration;
import aksharawekshakaya.config.PropertyConfig;
import aksharawekshakaya.util.Normalizer;

public class UserDictionary extends Lexicon {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2147376280587402935L;
	private boolean ready;

	public UserDictionary(File entryFile) { //additionally can mutate a dictionary file
		super(entryFile);
		ready = true;
	}
	
	public boolean addEntry(String word) {
		Normalizer norm = new Normalizer(" ");
		boolean successfull = wordList.add(norm.normalize(word));
		if(successfull) {
			save();
			return true;
		}
		else
			return false;
	}

	public boolean addEntrySet(Collection<String> entrySet) {
		Normalizer norm = new Normalizer(" ");
		List<String> list = new ArrayList<String>();
		for(String s : entrySet) {
			list.add(norm.normalize(s));
		}
		boolean successfull = wordList.addAll(list);
		if(successfull) {
			save();
			return true;
		}
		else
			return false;
	}
	
	public void save() {
		StringBuilder sb = new StringBuilder();
		BufferedWriter out = null;
		List<String> words = null;
		
		//entryList.delete();
		words  = new ArrayList<String>();
		words.addAll(wordList);
		Collections.sort(words);
		out = FileManager.getWriter(entryList);
		for(String word : words) {
			sb.append(word).append("\n");
		}
		try {
			out.write(sb.toString());
			PropertyConfig configs = PropertyConfig.getInstance();
			FileManager.writeObject(this, new File((configs.getProperty((Configuration.DICTIONARY_PATH), " "))+
					configs.getProperty((Configuration.USER_DIC), " ")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean removeEntry(String word) {
		boolean successfull = wordList.remove(word);
		if(successfull) {
			save();
			return true;
		}
		else
			return false;
	}
	
	@Override
	public boolean isReady() {
		return ready;
	}


}
