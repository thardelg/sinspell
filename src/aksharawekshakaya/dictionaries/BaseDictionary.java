package aksharawekshakaya.dictionaries;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import aksharawekshakaya.config.DefaultConfig;
import aksharawekshakaya.config.PropertyConfig;
import aksharawekshakaya.util.Normalizer;

public abstract class BaseDictionary implements Dictionary, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4022594685882795397L;
	protected File entryList;
	protected transient FileManager fileManager;
	private boolean ready;
	protected String encoding;
	protected transient PropertyConfig config;//FIXME suspicious of transient
	protected transient Normalizer normalizer;
	
	protected BaseDictionary(File entryFile){
		if(entryFile == null) {
			return;
		}
		fileManager = FileManager.getInstance();
		config = PropertyConfig.getInstance();
		encoding = config.getProperty(DefaultConfig.DEFAULT_ENCODING, "UTF-16");
		setFile(entryFile);
		normalizer = new Normalizer("");
		ready = false;
	}
	@Override
	public boolean setFile(File entryFile) {
		if(entryFile.exists()){
			this.entryList = entryFile;
			return true;
		} else {
			 try {
				entryFile.createNewFile();
				this.entryList = entryFile;
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return false;
		}
	}

	@Override
	public File getFile() {
		return this.entryList;
	}

	@Override
	public String toString() {
		return "File Name : " + entryList.getName();
	}

	@Override
	public boolean isReady() {
		return ready;
	}

}
