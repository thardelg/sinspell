package aksharawekshakaya.dictionaries;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
//import java.nio.charset.Charset;

import aksharawekshakaya.config.Configuration;
import aksharawekshakaya.config.PropertyConfig;

public class FileManager {

	protected static FileManager instance;
	public static String encoding;
	protected PropertyConfig config;

	protected FileManager() {
		config = PropertyConfig.getInstance();
		encoding = config.getProperty(Configuration.DEFAULT_ENCODING, "UTF-16");
	}

	public static FileManager getInstance() {
		if (instance == null)
			instance = new FileManager();
		return instance;
	}

	public static File getFile(File file) {
		if (file.exists()) {
			return file;
		} else {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return file;
		}
	}

	public static BufferedReader getReader(File file) {
		FileInputStream fis = null;
		BufferedReader in = null;
		Reader reader = null;
		getFile(file);
		try {
			fis = new FileInputStream(file);
			//reader = new InputStreamReader(fis, Charset.forName(encoding));
			reader = new InputStreamReader(fis, "UTF-16");
			in = new BufferedReader(reader);
			return in;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static BufferedWriter getWriter(File file) {
		OutputStreamWriter writer = null;
		FileOutputStream fos = null;
		BufferedWriter out = null;
		getFile(file);
		try {
			fos = new FileOutputStream(file);
			//writer = new OutputStreamWriter(fos, Charset.forName(encoding));
			writer = new OutputStreamWriter(fos, "UTF-16");
			out = new BufferedWriter(writer);
			return out;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return out;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return out;
		}
	}

	public static void writeObject(Object obj, File file) { // writes a serializable  object
		file = getFile(file);
		file.delete();
		OutputStream outs = null;
		OutputStream buffer = null;
		ObjectOutput output = null;
		try {
			file.createNewFile();
			outs = new FileOutputStream(file);
			buffer = new BufferedOutputStream(outs);
			output = new ObjectOutputStream(buffer);
			output.writeObject(obj);
			output.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static Object readObject(File file) { // reads a serializable object
		file = getFile(file);
		Object obj = null;
		InputStream in = null;
		InputStream buffer = null;
		ObjectInput input = null;
		try {
			in = new FileInputStream(file);
			buffer = new BufferedInputStream(in);
			input = new ObjectInputStream(buffer);
			obj = input.readObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return obj;
	}
}
