package aksharawekshakaya.dictionaries;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import aksharawekshakaya.util.TextUtils;


public class KeyBasedDictionary extends BaseDictionary {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5741263472031902975L;
	private boolean ready;
	protected Map<String, Double> codeMap;

	public KeyBasedDictionary(File entryFile) {
		super(entryFile);
		ready = false;
		codeMap = new TreeMap<String ,Double>(); //sorts all contents (so that aggregated contents are also sorted automatically)
		load();
	}
	
	public KeyBasedDictionary(File entryFile, Map<String, Double> distribution) {
		super(null);
		ready = false;
		codeMap = new TreeMap<String ,Double>(distribution); //sorts all contents (so that aggregated contents are also sorted automatically)
		ready = true;
	}
	
	@Override
	public void load() {
		BufferedReader in = FileManager.getReader(entryList);
		String s;
		try {
			s = in.readLine();
			if(s!=null) {
				s = normalizer.normalize(s); 
			}
			while(s != null) {
				if(!s.contains("\t")) {
					System.out.println("Invalid file format!");
					return;
				}
				double value = TextUtils.getDouble(s);
				String wkey = TextUtils.getStringKey(s);
				codeMap.put(wkey, value);
				s = in.readLine();
				if(s!=null) {
					s = normalizer.normalize(s); 
				}
			}
			ready = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean isReady() {
		return ready;
	}
	
	@Override
	public boolean contains(String word) {
		return codeMap.containsKey(word);
	}

	@Override
	public Set<String> getAll() {
		return codeMap.keySet();
	}

	@Override
	public int size() {
		return codeMap.size();
	}
	
	public double getValue(String word) {
		Double val = codeMap.get(word);
		if(val == null) {
			return 0.0;
		}else {
			return val;
		}
	}
	
	public Map<String, Double> getCodeMap() {
		return codeMap;
	}

}
