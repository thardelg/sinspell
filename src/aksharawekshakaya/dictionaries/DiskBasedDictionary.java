package aksharawekshakaya.dictionaries;

import java.io.BufferedWriter;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import aksharawekshakaya.util.Normalizer;

public class DiskBasedDictionary extends KeyBasedDictionary {

	/**
	 * TODO : DID CHANGE INT TO LONG IN INDEX..MAY RESULT IN AN ERROR + NOMALIZE ALL
	 * USED TO ACCESS LARGE NGRAM COUNTS EFFICIENTLY
	 */
	private static final long serialVersionUID = 5185535258619128146L;
	private boolean ready;
	protected String indexFilePath;
	protected String dataFilesPath;
	Map<String, Double> index; 
	Map<String, ArrayList<String>> wordSets; 
	
	public DiskBasedDictionary(File entryFile, String indexFilePath, String dataFilesPath, boolean bildOnStart) {
		super(entryFile);
		System.out.println("DONE KEYBASED!");
		ready = false;
		this.indexFilePath = indexFilePath;
		this.dataFilesPath = dataFilesPath;
		index = new HashMap<String, Double>(); 
		wordSets = new HashMap<String, ArrayList<String>>(); 
		if(bildOnStart)
			buildDataFiles();
	}
	
	
	
	protected void buildDataFiles() { //builds word file set
		for(Entry<String, Double> s : codeMap.entrySet()) {
			String code = getCode(s.getKey());
			ArrayList<String> codedList = null;
			if(code.equals(s.getKey())) {
				codedList = wordSets.get("0");
				if(codedList == null) {
					codedList = new ArrayList<String>();
					codedList.add(s.getKey() +"\t"+ s.getValue());
					wordSets.put("0", codedList);
				}else {
					codedList.add(s.getKey() +"\t"+ s.getValue());
					wordSets.put("0", codedList);
				}
			}
			else{
				if(wordSets.containsKey(code)) {
					codedList = wordSets.get(code);
					codedList.add(s.getKey() +"\t"+ s.getValue());
					wordSets.put(code, codedList);
				} else {
					codedList = new ArrayList<String>();
					codedList.add(s.getKey() +"\t"+ s.getValue());
					wordSets.put(code, codedList);
				}
			}
			codedList = null;
		}
		System.out.println("DONE LOADING COLLECTIONS!");
		double i = 0; 
		try{
			System.out.println("STARTED WRITING....");
			for(Entry<String, ArrayList<String>> s : wordSets.entrySet()) {
				index.put(s.getKey(), i);
				BufferedWriter out = FileManager.getWriter(new File(dataFilesPath + (int)i + ".txt"));
				StringBuilder sb = new StringBuilder();
				for(String line : s.getValue()) {
					sb.append(line).append("\n");
				}
				out.write(sb.toString());
				out.close();
				i++;
			}
			System.out.println("DONE WRITING DATABASE!");
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		buildIndexFile();
		ready = true;
		wordSets.clear(); // remove wordset contents after writing to file
	}
	
	//it takes time to create the index....be carefull about the file size
	protected void buildIndexFile() { //build Index file containing word Files
		try{
			BufferedWriter out = FileManager.getWriter(new File(indexFilePath + "index.idx"));
			StringBuilder sb = new StringBuilder();
			for(Entry<String, Double> s : index.entrySet()) {
				sb.append(s.getKey()).append("\t").append(s.getValue()).append("\n");
			}
			out.write(sb.toString());
			out.close();
			System.out.println("DONE WRITING INDEX!");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	protected void loadIndexFile() { //build Index file containing word Files
			if(this.index.isEmpty()) {
				KeyBasedDictionary indexSet = new KeyBasedDictionary(new File(indexFilePath + "index.idx")); 
				index = indexSet.codeMap;
			}
	}

	protected String getCode(final String word) {
		Normalizer n = new Normalizer("");
		String code = n.normalize(word);
		if(code.length()<3){
			return word;
		}else {
			return code.substring(0, 3);
		}
		
	}
	
	public double getValue(String key) { //returns 0 if not found
		long val = 0;
		String code = getCode(key);
		String file = index.get(code).toString();
		try{
			loadIndexFile();
			RandomAccessor ran = new RandomAccessor(new File(dataFilesPath + file + ".txt"));
			val =  ran.getFreq(key);
		} catch(Exception e){
			e.printStackTrace();
		}
		return val;
	}
	
	public boolean isReady() {
		return ready;
	}

}