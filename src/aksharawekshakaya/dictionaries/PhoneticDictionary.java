package aksharawekshakaya.dictionaries;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import aksharawekshakaya.spellengine.PhoneticTransformator;
import aksharawekshakaya.spellengine.SoundexTransformator;

public class PhoneticDictionary extends Lexicon {

	/**
	 * 
	 */
	//FIXME remove wordList overhead --remove overhead from all sub dictionaries
	private static final long serialVersionUID = -7928731269706433913L;
	private boolean ready;
	protected Map<String, HashSet<String>> codeMap;
	protected PhoneticTransformator transformator;
	protected int span;
	
	public PhoneticDictionary(File entryFile, final int span) {
		super(entryFile);
		while(!super.isReady());
		ready = false;
		this.span = span;
		transformator = new SoundexTransformator(" ", span);
		codeMap = new HashMap<String, HashSet<String>>();
		for (String word : wordList) {
	    	 final String code = getCode(word);
	    	 HashSet<String> similarList = codeMap.get(code);
	    	 if(similarList == null) {
	    		 similarList = new HashSet<String>();
	    		 similarList.add(word);
	    	 } else {
	    		 similarList.add(word);
	    	 }
	    	 codeMap.put(code, similarList);
	     }
		ready = true;
	}
	
	@Override
	public boolean isReady() {
		return ready;
	}
	
	public Set<String> getSimilarSet(String word) {
		String code = getCode(word);
		if (codeMap.get(code) == null) {
			return new HashSet<String>();
		} else {
			return codeMap.get(code);
		}
	}
	
	public String getCode(String word) {
		return transformator.transform(word);
	}
	
	public void setSpan(int span) {
		this.span = span;
	}
	
	public int getSpan() {
		return span;
	}
	
	public Map<String, HashSet<String>> getCodeMap() {
		return codeMap;
	}
}
