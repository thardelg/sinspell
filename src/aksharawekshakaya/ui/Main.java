package aksharawekshakaya.ui;


import java.math.BigDecimal;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class Main {

	protected Shell shlAksharawekshakaya;
	protected Controller controller;
	protected StyledText context;
	List lstMisspelled;
	List lstSuggestions;
	Label lbl_words ;
	Label lbl_mispelled;
	Label lbl_correct;
	Label lbl_acc;
	Button btnAddToDic;
	Button btnCorrect;
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {

			Main window = new Main();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlAksharawekshakaya.open();
		shlAksharawekshakaya.layout();
		while (!shlAksharawekshakaya.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlAksharawekshakaya = new Shell();
		shlAksharawekshakaya.setSize(950, 705);
		shlAksharawekshakaya.setText("Aksharawekshakaya (අක්ෂරාවේක්ෂකය)");
		
		//MY CODE
        controller = new Controller();

		
		TabFolder tabFolder = new TabFolder(shlAksharawekshakaya, SWT.NONE);
		tabFolder.setBounds(0, 10, 942, 641);
		
		TabItem tbtmSpellChecker = new TabItem(tabFolder, SWT.NONE);
		tbtmSpellChecker.setText("Spell Checker");
		
		Composite composite = new Composite(tabFolder, SWT.NONE);
		composite.setFont(SWTResourceManager.getFont("Impact", 14, SWT.NORMAL));
		tbtmSpellChecker.setControl(composite);
		
		lstSuggestions = new List(composite, SWT.BORDER | SWT.V_SCROLL);
		lstSuggestions.setFont(SWTResourceManager.getFont("Iskoola Pota", 12, SWT.NORMAL));
		lstSuggestions.setBounds(766, 222, 158, 159);
		
		//ADDED HERE
		lbl_words = new Label(composite, SWT.NONE);
		lbl_words.setBounds(854, 453, 70, 13);
		lbl_words.setText(" ");
		
		lbl_mispelled = new Label(composite, SWT.NONE);
		lbl_mispelled.setBounds(854, 472, 70, 13);
		lbl_mispelled.setText(" ");
		
		lbl_correct = new Label(composite, SWT.NONE);
		lbl_correct.setBounds(854, 491, 70, 13);
		lbl_correct.setText(" ");
		
		lbl_acc = new Label(composite, SWT.NONE);
		lbl_acc.setText(" ");
		lbl_acc.setBounds(854, 510, 70, 13);
		
		lstMisspelled = new List(composite, SWT.BORDER | SWT.V_SCROLL);
		lstMisspelled.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(lstMisspelled.getSelectionIndex()>=0) {
					String miss = lstMisspelled.getItem(lstMisspelled.getSelectionIndex());
					lstSuggestions.removeAll();
					//Main.this.context.setText(controller.getContext()); /////
					if(miss != null ) {
						
						//lstMisspelled.add(miss);//
						StyleRange s1 = new StyleRange();//
						s1.start = context.getText().indexOf(miss);//
						s1.length = miss.length();//
						s1.background = shlAksharawekshakaya.getDisplay().getSystemColor(SWT.COLOR_RED);
						s1.foreground = shlAksharawekshakaya.getDisplay().getSystemColor(SWT.COLOR_WHITE);
						s1.underlineColor =  shlAksharawekshakaya.getDisplay().getSystemColor(SWT.COLOR_RED);//
						String pp = context.getText();
						if((s1.start>=0) && ( (s1.start+s1.length) <= (pp.length()-1) )) {
							Main.this.context.setStyleRange(s1);
							}
						
						if(controller.getSuggestionsList().containsKey(miss)) {
							for(String s : controller.getSuggestionsList().get(miss)) {
								if(s!=null) {
									lstSuggestions.add(s);
								}
								
							}
						}
				}
				
					
//					int total = controller.getTokenSet().size();
//					int msp = controller.getMispelledList().size();
//					lblMispelled.setText(String.valueOf(msp));
//					lblCorrect.setText(String.valueOf(total - msp));
//					lblWords.setText(String.valueOf(total));
					
				}
				
				
			}
		});
		lstMisspelled.setFont(SWTResourceManager.getFont("Iskoola Pota", 12, SWT.NORMAL));
		lstMisspelled.setBounds(766, 31, 158, 137);
		
		context = new StyledText(composite, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL |SWT.H_SCROLL );
		context.setTopMargin(12);
		context.setRightMargin(10);
		context.setLeftMargin(10);
		context.setFont(SWTResourceManager.getFont("Iskoola Pota", 12, SWT.NORMAL));
		context.setBounds(0, 10, 760, 603);
		context.setText(" ");

		
		Button btnSpellCheck = new Button(composite, SWT.NONE);
		btnSpellCheck.setImage(SWTResourceManager.getImage("C:\\Documents\\APIIT Projects\\Level-3\\Semester - 2\\FYP\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\icons\\1362469554_9593.ico"));
		btnSpellCheck.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lstSuggestions.removeAll();
				
				lbl_mispelled.setText(" ");
				lbl_correct.setText(" ");
				lbl_words.setText(" ");
				
				Controller cont = Main.this.controller;
				String source = (Main.this.context.getText());
				source = source.trim() + " \t";
				
				if((source == null) || source.isEmpty()) {
					source = " ";
				}
				cont.spellCheck(source);
				Main.this.context.setText(cont.getContext());
				lstMisspelled.removeAll();
				for(String s : cont.getMispelledList()) {
					lstMisspelled.add(s);
					StyleRange s1 = new StyleRange();
					s1.start = context.getText().indexOf(s);
					s1.length = s.length();
					s1.underline = true;
					s1.underlineStyle = 3;
					s1.underlineColor =  shlAksharawekshakaya.getDisplay().getSystemColor(SWT.COLOR_RED);
					String pp = context.getText();
					if((s1.start>=0) && ( (s1.start+s1.length) <= (pp.length()-1) )) {
						Main.this.context.setStyleRange(s1);
						}
				}
				
				int total = controller.getTokenSet().size();
				int mspss = controller.getMispelledList().size();
				lbl_mispelled.setText(String.valueOf(mspss));
				lbl_correct.setText(String.valueOf(total - mspss));
				lbl_words.setText(String.valueOf(total));
				BigDecimal bd = new BigDecimal((double)(((double)(total - mspss)/(double)total)*100.0));
				lbl_acc.setText(String.valueOf(bd.floatValue()));
				btnCorrect.setEnabled(true);
		        btnAddToDic.setEnabled(true);
				
			}
		});
		btnSpellCheck.setBounds(766, 567, 158, 36);
		btnSpellCheck.setText("Spell Check");
		
		Label lblMispelledWords = new Label(composite, SWT.NONE);
		lblMispelledWords.setBounds(766, 10, 103, 23);
		lblMispelledWords.setText("Mispelled Words : ");
		
		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setBounds(766, 203, 103, 23);
		lblNewLabel.setText("Suggestions List :");
		
		btnAddToDic = new Button(composite, SWT.NONE);
		btnAddToDic.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(lstMisspelled.getSelectionIndex()>=0) {
					String miss = lstMisspelled.getItem(lstMisspelled.getSelectionIndex());
					controller.addToUserDic(miss);
					
					lstSuggestions.removeAll();
					Controller cont = Main.this.controller;
					String source = (Main.this.context.getText());
					source = source.trim();
					
					if((source == null) || source.isEmpty()) {
						source = " ";
					}
					cont.spellCheck(source);
					Main.this.context.setText(cont.getContext());
					lstMisspelled.removeAll();
					for(String s : cont.getMispelledList()) {
						lstMisspelled.add(s);
						StyleRange s1 = new StyleRange();
						s1.start = context.getText().indexOf(s);
						s1.length = s.length();
						s1.underline = true;
						s1.underlineStyle = 3;
						s1.underlineColor =  shlAksharawekshakaya.getDisplay().getSystemColor(SWT.COLOR_RED);
						String pp = context.getText();
						if((s1.start>=0) && ( (s1.start+s1.length) <= (pp.length()-1) )) {
							Main.this.context.setStyleRange(s1);
							}
						}
					}
					
//					int total = controller.getTokenSet().size();
//					int msp = controller.getMispelledList().size();
//					lblMispelled.setText(String.valueOf(msp));
//					lblCorrect.setText(String.valueOf(total - msp));
//					lblWords.setText(String.valueOf(total));
					
					
				}
				
			
		});
		btnAddToDic.setBounds(766, 174, 158, 23);
		btnAddToDic.setText("Add to Dictionary");
		
		btnCorrect = new Button(composite, SWT.NONE);
		btnCorrect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(lstSuggestions.getSelectionIndex() >= 0) {
					if(lstMisspelled.getSelectionIndex() >= 0) {
						String misspell = lstMisspelled.getItem(lstMisspelled.getSelectionIndex());
						String suggestion = lstSuggestions.getItem(lstSuggestions.getSelectionIndex());
						String old = context.getText();
						context.setText(old.replaceAll(misspell, suggestion));
						
						lstMisspelled.remove(misspell);
						lstSuggestions.removeAll();
						Controller cont = Main.this.controller;
						for(String s : cont.getMispelledList()) {
							if(!s.equals(misspell)) {
							StyleRange s1 = new StyleRange();
							s1.start = context.getText().indexOf(s);
							s1.length = s.length();
							s1.underline = true;
							s1.underlineStyle = 3;
							s1.underlineColor =  shlAksharawekshakaya.getDisplay().getSystemColor(SWT.COLOR_RED);
							String pp = context.getText();
							if((s1.start>=0) && ( (s1.start+s1.length) <= (pp.length()-1) )) {
								Main.this.context.setStyleRange(s1);
								}
							}
						}
//						Controller cont = Main.this.controller;
//						String source = (Main.this.context.getText());
//						source = source.trim();
//						
//						if((source == null) || source.isEmpty()) {
//							source = " ";
//						}
//						cont.spellCheck(source);
//						Main.this.context.setText(cont.getContext());
//						lstMisspelled.removeAll();
//						for(String s : cont.getMispelledList()) {
//							lstMisspelled.add(s);
//							StyleRange s1 = new StyleRange();
//							s1.start = context.getText().indexOf(s);
//							s1.length = s.length();
//							s1.underline = true;
//							s1.underlineStyle = 3;
//							s1.underlineColor =  shlAksharawekshakaya.getDisplay().getSystemColor(SWT.COLOR_RED);
//							Main.this.context.setStyleRange(s1);
//						}
						
//						int total = controller.getTokenSet().size();
//						int msp = controller.getMispelledList().size();
//						lblMispelled.setText(String.valueOf(msp));
//						lblCorrect.setText(String.valueOf(total - msp));
//						lblWords.setText(String.valueOf(total));
					}
				}
			}
		});
		btnCorrect.setBounds(766, 387, 158, 23);
		btnCorrect.setText("Correct");
		
		btnCorrect.setEnabled(false);
        btnAddToDic.setEnabled(false);
		
		Label lblWords = new Label(composite, SWT.NONE);
		lblWords.setBounds(766, 453, 82, 13);
		lblWords.setText("Sinhala Words :");
		
		Label lblMispelled = new Label(composite, SWT.NONE);
		lblMispelled.setBounds(766, 472, 82, 13);
		lblMispelled.setText("Mispelled         :");
		
		Label lblCorrect = new Label(composite, SWT.NONE);
		lblCorrect.setBounds(766, 491, 82, 13);
		lblCorrect.setText("Correct             :");
		
		Label label = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setBounds(766, 445, 158, 2);
		
		Label label_1 = new Label(composite, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_1.setBounds(766, 540, 158, 2);
		
		Label lblStatistics = new Label(composite, SWT.NONE);
		lblStatistics.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblStatistics.setBounds(815, 427, 55, 15);
		lblStatistics.setText("Statistics");
		
		Label lblAccuracy = new Label(composite, SWT.NONE);
		lblAccuracy.setText("Accuracy(%)    :");
		lblAccuracy.setBounds(766, 510, 82, 24);
		
		
		//TAKEN FROM HERE *******************************************
		
		
		TabItem tbtmNewItem = new TabItem(tabFolder, SWT.NONE);
		tbtmNewItem.setText("Test Suite");
		
		Menu menu = new Menu(shlAksharawekshakaya, SWT.BAR);
		shlAksharawekshakaya.setMenuBar(menu);
		
		MenuItem mntmDd = new MenuItem(menu, SWT.CASCADE);
		mntmDd.setText("File");
		
		Menu menu_1 = new Menu(mntmDd);
		mntmDd.setMenu(menu_1);
		
		MenuItem menuOpen = new MenuItem(menu_1, SWT.NONE);
		menuOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(shlAksharawekshakaya, SWT.OPEN);
		        fd.setText("Open");
		        fd.setFilterPath("C:/");
		        String[] filterExt = { "*.txt" };
		        fd.setFilterExtensions(filterExt);
		        String selected = fd.open();
		        if((selected == null) || (selected == "") || (selected.length()<=0)) {
		        	return;
		        }
		        String fileContents = controller.openFile(selected);
		        Main.this.context.setText(fileContents);
		        lstMisspelled.removeAll();
		        lstSuggestions.removeAll();
		        btnCorrect.setEnabled(false);
		        btnAddToDic.setEnabled(false);
			}
		});
		menuOpen.setText("Open");
		
		MenuItem mntmView = new MenuItem(menu, SWT.CASCADE);
		mntmView.setText("View");
		
		Menu menu_2 = new Menu(mntmView);
		mntmView.setMenu(menu_2);
		
		MenuItem menuHelp = new MenuItem(menu_2, SWT.NONE);
		menuHelp.setText("Help");

	}
}
