package aksharawekshakaya.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import aksharawekshakaya.dictionaries.DictionaryManager;
import aksharawekshakaya.dictionaries.FileManager;
import aksharawekshakaya.dictionaries.UserDictionary;
import aksharawekshakaya.spellchecker.SpellChecker;
import aksharawekshakaya.spellengine.Suggestion;

public class Controller {
	
	private SpellChecker spellChecker;
	private DictionaryManager dm;
	ArrayList<String> mispelledList;
	Map<String, List<String>> suggestionsList;
	
	
	public Controller() {
		dm = DictionaryManager.getInstance();
		dm.getLexicon();
		dm.getPhoneticAbslDictionary();
		dm.getPhoneticDictionary();
		dm.getsyBiGramDic();
		dm.getUserDictionary();
		mispelledList = new ArrayList<String>();
		suggestionsList = new HashMap<String, List<String>>();
	}
	
	public void spellCheck(String source) {
		mispelledList.clear();
		suggestionsList.clear();
		spellChecker = new SpellChecker(source, dm);
		spellChecker.spellCheck();
		mispelledList.addAll(spellChecker.getRequest().getErrorSet());
		Map<String, Set<Suggestion>> sugs = spellChecker.getRequest().getSuggestionsMap();
		Map<String, List<String>> allsugs = new HashMap<String, List<String>>();
		
		for(Entry<String, Set<Suggestion>> entry : sugs.entrySet()) {
			List<String> ss = new ArrayList<String>();
			for(Suggestion sug : entry.getValue()) {
				ss.add(sug.getSuggestion());
			}
			allsugs.put(entry.getKey(),ss);
		}
		
		suggestionsList.clear();
		suggestionsList = allsugs;
	}


	public ArrayList<String> getMispelledList() {
		return mispelledList;
	}

	public Map<String, List<String>> getSuggestionsList() {
		return suggestionsList;
	}
	
	public String openFile(String path) {
		File f = FileManager.getFile(new File(path));
		StringBuilder sb = new StringBuilder();
		BufferedReader in = FileManager.getReader(f);
		String s = "";
		try {
			s = in.readLine();
			while(s!=null) {
				sb.append(s.trim()).append("\n");
				s = in.readLine();  
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public String getContext() {
		return spellChecker.getRequest().getContext();
	}
	
	public Set<String> getTokenSet() {
		return spellChecker.getRequest().getTokenSet();
	}
	
	public void addToUserDic(String word) {
		UserDictionary udic = dm.getUserDictionary();
		udic.addEntry(word);
		dm.reloadUserDictionary();
	}
}
