package aksharawekshakaya.spellchecker;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import aksharawekshakaya.dictionaries.TrieBasedDictionary;
import aksharawekshakaya.spellengine.GenericSuggester;
import aksharawekshakaya.spellengine.Suggestion;
import aksharawekshakaya.spellengine.TrieBasedSuggester;

public class TrieSuggestionHandler extends SuggestionHandler {

	protected GenericSuggester genSug;
	public TrieSuggestionHandler(SuggestionHandler next, TrieBasedDictionary dictionary, int distance) {
		super(next);
		genSug = new TrieBasedSuggester(dictionary, distance);
	}
	
	@Override
	public void handle(SpellSuggester spellSuggester) {
		Set<String> errorSet = spellSuggester.getErrorSet();
		Map<String, Set<Suggestion>> suggestionsMap = spellSuggester.getSuggestionsMap();
		
		for(String misspelled : errorSet) {
			List<Suggestion> sugs = genSug.suggest(misspelled);
			if((sugs == null) || sugs.isEmpty()) {
				continue;
			} else {
				if(suggestionsMap.containsKey(misspelled)) {
					Set<Suggestion> existing = suggestionsMap.get(misspelled);
					existing.addAll(sugs);
					suggestionsMap.put(misspelled, existing);
				} else {
					Set<Suggestion> newSugs = new LinkedHashSet<Suggestion>(sugs);
					suggestionsMap.put(misspelled, newSugs);
				}
				
			}
		}
		spellSuggester.setSuggestionsMap(suggestionsMap);
		super.handle(spellSuggester);
	}

}
