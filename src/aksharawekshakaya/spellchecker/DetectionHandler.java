package aksharawekshakaya.spellchecker;

import aksharawekshakaya.spellengine.GenericDetector;

public abstract class DetectionHandler {
	protected GenericDetector detector;
	protected DetectionHandler next;
	
	public DetectionHandler(DetectionHandler next) {
		this.next = next;
	}
	
	public void handle(SpellDetector spellDetector) {
		next.handle(spellDetector);
	}

	public GenericDetector getDetector() {
		return detector;
	}

	public void setDetector(GenericDetector detector) {
		this.detector = detector;
	}

	public DetectionHandler getNext() {
		return next;
	}

	public void setNext(DetectionHandler next) {
		this.next = next;
	}
}
