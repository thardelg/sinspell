package aksharawekshakaya.spellchecker;

import java.util.Set;

import aksharawekshakaya.dictionaries.KeyBasedDictionary;
import aksharawekshakaya.ngram.NgramType;
import aksharawekshakaya.spellengine.GenericDetector;
import aksharawekshakaya.spellengine.NgramBasedDetector;

public class NgramBasedHandler extends DetectionHandler {
	
	protected GenericDetector genDic;
	public NgramBasedHandler(DetectionHandler next, KeyBasedDictionary dictionary, NgramType type, double threshold, double offset) {
		super(next);
		genDic = new NgramBasedDetector(dictionary, type, threshold, offset);
	}
	
	@Override
	public void handle(SpellDetector spellDetector) {
		Set<String> tokenSet = spellDetector.getTokenSet();
		Set<String> errorSet = spellDetector.getErrorSet();
		for(String word : tokenSet) {
			if(!genDic.detect(word)) {
				errorSet.add(word);
			}
		}
		spellDetector.setErrorSet(errorSet);
		super.handle(spellDetector);
	}
	
}
