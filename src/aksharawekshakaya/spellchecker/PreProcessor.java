package aksharawekshakaya.spellchecker;

import java.util.LinkedHashSet;
import java.util.Set;

import aksharawekshakaya.util.Normalizer;
import aksharawekshakaya.util.StringTokenizer;
import aksharawekshakaya.util.Token;
import aksharawekshakaya.util.WordToken;

public class PreProcessor extends SpellHandler {

	protected Set<String> tokenSet;
	protected StringTokenizer tokenizer;
	protected Normalizer normalizer;
	
	public PreProcessor(SpellHandler next) {
		super(next);
		tokenSet = new LinkedHashSet<String>();
		normalizer = new Normalizer(" ");
	}
	
	@Override
	public void handle(SpellRequest request) {
		preprocess(request.getContext());
		request.setTokenSet(tokenSet);
		System.out.println("TOKENIZED!");
		super.handle(request);
	}
	
	public void preprocess(String context) {
		String normalizedContext = normalizer.normalize(context);
		tokenizer = new StringTokenizer(normalizedContext, new WordToken(0, " "));
		while(tokenizer.hasNext()) {
			Token t = tokenizer.next();
			if(t.isSinhala()) {
				tokenSet.add(t.value());
			}
		}
		
	}

}
