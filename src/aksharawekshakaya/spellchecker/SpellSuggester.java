package aksharawekshakaya.spellchecker;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import aksharawekshakaya.spellengine.Suggestion;


public class SpellSuggester extends SpellHandler {

	Set<String> errorSet;
	Map<String, Set<Suggestion>> suggestionsMap;
	SuggestionHandler suggester;
	private boolean isDone;
	
	public SpellSuggester(SpellHandler next, SuggestionHandler suggester) {
		super(next);
		errorSet = new LinkedHashSet<String>();
		suggestionsMap = new HashMap<String, Set<Suggestion>>();
		this.suggester = suggester;
		isDone = false;
	}
	
	@Override
	public void handle(SpellRequest request) {
		this.errorSet = request.getErrorSet();
		suggester.handle(this);
		request.setSuggestionsMap(suggestionsMap);
		while(!this.isDone());
		super.handle(request);
	}

	public Set<String> getErrorSet() {
		return errorSet;
	}

	public void setErrorSet(Set<String> errorSet) {
		this.errorSet = errorSet;
	}

	public Map<String, Set<Suggestion>> getSuggestionsMap() {
		return suggestionsMap;
	}

	public void setSuggestionsMap(Map<String, Set<Suggestion>> suggestionsMap) {
		this.suggestionsMap = suggestionsMap;
	}

	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}


}
