package aksharawekshakaya.spellchecker;

import java.util.Set;

import aksharawekshakaya.dictionaries.Dictionary;
import aksharawekshakaya.spellengine.DictionaryBasedDetector;
import aksharawekshakaya.spellengine.GenericDetector;

public class DictionaryBasedHandler extends DetectionHandler {

	protected GenericDetector genDic;
	protected Dictionary dic;
	public DictionaryBasedHandler(DetectionHandler next, Dictionary dictionary) {
		super(next);
		dic = dictionary;
		genDic = new DictionaryBasedDetector(dictionary);
	}
	
	@Override
	public void handle(SpellDetector spellDetector) {
		Set<String> tokenSet = spellDetector.getTokenSet();
		Set<String> errorSet = spellDetector.getErrorSet();
		for(String word : tokenSet) {
			if(dic.getClass().getSimpleName().equals("UserDictionary")) {
				//add user dictionary handler as the last one
				if(errorSet.contains(word) && genDic.detect(word)) { 
					//if error set contains user word and user dictionary contains that word
					errorSet.remove(word);
				}
			} else if (!genDic.detect(word)) {
				errorSet.add(word);
			} 
		}
		spellDetector.setErrorSet(errorSet);
		if(dic.getClass().getSimpleName().equals("UserDictionary")) {
			spellDetector.setDone(true);
			return;
		}
		super.handle(spellDetector);
	}
	
}
