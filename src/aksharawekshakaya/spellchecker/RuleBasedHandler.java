package aksharawekshakaya.spellchecker;

import java.util.Set;

import aksharawekshakaya.spellengine.GenericDetector;
import aksharawekshakaya.spellengine.RuleBasedDetector;

public class RuleBasedHandler extends DetectionHandler {
	
	protected GenericDetector genDic;
	public RuleBasedHandler(DetectionHandler next) {
		super(next);
		genDic = new RuleBasedDetector();
	}
	
	@Override
	public void handle(SpellDetector spellDetector) {
		Set<String> tokenSet = spellDetector.getTokenSet();
		Set<String> errorSet = spellDetector.getErrorSet();
		for(String word : tokenSet) {
			if(!genDic.detect(word)) {
				errorSet.add(word);
			}
		}
		spellDetector.setErrorSet(errorSet);
		super.handle(spellDetector);
	}
	
}
