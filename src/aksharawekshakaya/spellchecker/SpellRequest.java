package aksharawekshakaya.spellchecker;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import aksharawekshakaya.spellengine.Suggestion;

public class SpellRequest {
	private String context;
	private Set<String> tokenSet;
	private Set<String> errorSet;
	private Map<String, Set<Suggestion>> suggestionsMap;
	private boolean isCompleted;
	private SpellChecker spc;
	
	public SpellRequest(String context, SpellChecker spc) {
		this.context = context;
		tokenSet = new LinkedHashSet<String>();
		errorSet = new LinkedHashSet<String>();
		suggestionsMap = new HashMap<String, Set<Suggestion>>();
		this.spc = spc;
		isCompleted = false;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public Set<String> getTokenSet() {
		return tokenSet;
	}

	public void setTokenSet(Set<String> tokenSet) {
		this.tokenSet = tokenSet;
	}

	public Set<String> getErrorSet() {
		return errorSet;
	}

	public void setErrorSet(Set<String> errorSet) {
		this.errorSet = errorSet;
	}

	public Map<String, Set<Suggestion>> getSuggestionsMap() {
		return suggestionsMap;
	}

	public void setSuggestionsMap(Map<String, Set<Suggestion>> suggestionsMap) {
		this.suggestionsMap = suggestionsMap;
	}

	public boolean isCompleted() {
		return isCompleted;
	}

	public void setCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}

	public SpellChecker getSpc() {
		return spc;
	}

	public void setSpc(SpellChecker spc) {
		this.spc = spc;
	}
	
	
}
