package aksharawekshakaya.spellchecker;

public abstract class SpellHandler {
	protected SpellHandler next;
	
	public SpellHandler(SpellHandler next) {
		this.next = next;
	}
	
	public void handle(SpellRequest request) {
		next.handle(request);
	}

	public SpellHandler getNext() {
		return next;
	}

	public void setNext(SpellHandler next) {
		this.next = next;
	}

}
