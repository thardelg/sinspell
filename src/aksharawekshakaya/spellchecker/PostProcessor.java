package aksharawekshakaya.spellchecker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import aksharawekshakaya.spellengine.Suggestion;
import aksharawekshakaya.util.Normalizer;

public class PostProcessor extends SpellHandler {

	private String context;
	private Set<String> tokenSet;
	private Set<String> errorSet;
	private Map<String, Set<Suggestion>> suggestionsMap;
	protected Normalizer normalizer;
	
	public PostProcessor(SpellHandler next) {
		super(next);
		context = " ";
		tokenSet = new LinkedHashSet<String>();
		errorSet = new LinkedHashSet<String>();
		suggestionsMap = new HashMap<String, Set<Suggestion>>();
		normalizer = new Normalizer(" ");
	}
	
	@Override
	public void handle(SpellRequest request) {
		context = request.getContext();
		postProcess(request);
	}
	
	public void postProcess(SpellRequest request) {
		context = normalizer.normalize(request.getContext());
		context = Normalizer.replaceZJW(context);
		
		for(String token : request.getTokenSet()) {
			tokenSet.add(Normalizer.replaceZJW(token));
		}
		
		for(String mispelling : request.getErrorSet()) {
			errorSet.add(Normalizer.replaceZJW(mispelling));
		}
		
		for(Entry<String, Set<Suggestion>> entry : request.getSuggestionsMap().entrySet()) {
			String key = Normalizer.replaceZJW(entry.getKey());
			List<Suggestion> order = new ArrayList<Suggestion>();
			for(Suggestion s : entry.getValue()) {
//				Suggestion newSug = new Suggestion(null, Normalizer.replaceZJW(s.getSuggestion()), s.getScore());
//				sugs.add(newSug);
				order.add(new Suggestion(s.getWord(),Normalizer.replaceZJW(s.getSuggestion()),getEdits(key, 
						Normalizer.replaceZJW(s.getSuggestion()) )));
				Collections.sort(order, Collections.reverseOrder());
			}
			suggestionsMap.put(key, new LinkedHashSet<Suggestion>(order));
		}
		request.setContext(context);
		request.setErrorSet(errorSet);
		request.setSuggestionsMap(suggestionsMap);
		request.setTokenSet(tokenSet);
		request.setCompleted(true);
		request.getSpc().setRequest(request);
		request.getSpc().setReady(true);
	}
	
	public static int getEdits(CharSequence str1, 
            CharSequence str2) {
    int[][] distance = new int[str1.length() + 1][str2.length() + 1];

    for (int i = 0; i <= str1.length(); i++)
            distance[i][0] = i;
    for (int j = 1; j <= str2.length(); j++)
            distance[0][j] = j;

    for (int i = 1; i <= str1.length(); i++)
            for (int j = 1; j <= str2.length(); j++)
                    distance[i][j] = smallest(
                                    distance[i - 1][j] + 1,
                                    distance[i][j - 1] + 1,
                                    distance[i - 1][j - 1]
                                                    + ((str1.charAt(i - 1) == str2.charAt(j - 1)) ? 0
                                                                    : 1));

    return distance[str1.length()][str2.length()];
	}
	 private static int smallest(int a, int b, int c) {
         return Math.min(Math.min(a, b), c);
	 }

}
