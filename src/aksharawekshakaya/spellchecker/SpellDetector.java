package aksharawekshakaya.spellchecker;

import java.util.LinkedHashSet;
import java.util.Set;


public class SpellDetector extends SpellHandler {

	protected Set<String> errorSet;
	protected Set<String> tokenSet;
	protected DetectionHandler detector;
	private boolean isDone;
	
	public SpellDetector(SpellHandler next, DetectionHandler detector) {
		super(next);
		tokenSet =  new LinkedHashSet<String>();
		errorSet = new LinkedHashSet<String>();
		this.detector = detector;
		isDone = false;
	}
	
	@Override
	public void handle(SpellRequest request) {
		this.tokenSet = request.getTokenSet();
		detector.handle(this);
		//waiting till detection chain is traversed
		while(!this.isDone()); 
		request.setErrorSet(errorSet);
		super.handle(request);
	}

	public Set<String> getErrorSet() {
		return errorSet;
	}

	public void setErrorSet(Set<String> errorSet) {
		this.errorSet = errorSet;
	}

	public Set<String> getTokenSet() {
		return tokenSet;
	}

	public void setTokenSet(Set<String> tokenSet) {
		this.tokenSet = tokenSet;
	}

	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}

}
