package aksharawekshakaya.spellchecker;

import aksharawekshakaya.dictionaries.DictionaryManager;
import aksharawekshakaya.ngram.NgramType;
import aksharawekshakaya.spellengine.PhoneticSuggester;

public class SpellChecker {

	private DictionaryManager dictionaryManager;
	private SpellHandler spellHandler;
	private SpellRequest request;
	private boolean isReady;

	public SpellChecker(String context, DictionaryManager dictionaryManager) {
		this.request = new SpellRequest(context, this);
		this.dictionaryManager = dictionaryManager;
		setHandlerChain();
		this.isReady = false;
	}

	public void spellCheck() {
		spellHandler.handle(request);
		System.out.println("Getting ready!");
		while(!isReady);
		System.out.println("READY!");
	}

	public void generateSuggestions() {}

	public boolean isReady() {
		return isReady;
	}

	public SpellRequest getRequest() {
		return request;
	}

	public void setRequest(SpellRequest request) {
		this.request = request;
	}

	public void setHandlerChain() {

		// setting suggestion handler chain //WARNING : BREAKING THE CHAIN CAN RESULT IN WRONG FUNCTIONALTY
		PhoneticSuggester ps = new PhoneticSuggester(
				dictionaryManager.getPhoneticDictionary());
		NgramSuggestionHandler ngramHandler = new NgramSuggestionHandler(
				null, ps, 0.1);
		PhoneticSuggestionHandler phoneticHandler = new PhoneticSuggestionHandler(
				ngramHandler, dictionaryManager.getPhoneticDictionary());
		TrieSuggestionHandler trieHandler = new TrieSuggestionHandler(phoneticHandler,
				dictionaryManager.getLexicon(), 2);

		// setting detection handler chain //WARNING : BREAKING THE CHAIN CAN RESULT IN WRONG FUNCTIONALTY
		
		DictionaryBasedHandler userDefinedDetector = new DictionaryBasedHandler(
				null, dictionaryManager.getUserDictionary());
		NgramBasedHandler ngramDetector = new NgramBasedHandler(userDefinedDetector,
				dictionaryManager.getsyBiGramDic(), NgramType.SYLLABLE_BI,
				1, 1000000000);
		DictionaryBasedHandler lexDetector = new DictionaryBasedHandler(
				ngramDetector, dictionaryManager.getLexicon());
		RuleBasedHandler rbHandler = new RuleBasedHandler(lexDetector);

		//setting spell checking handler chain //WARNING : BREAKING THE CHAIN CAN RESULT IN WRONG FUNCTIONALTY
		PostProcessor postProcessor = new PostProcessor(null);
		SpellSuggester suggestionPhase = new SpellSuggester(postProcessor,
				trieHandler);
		SpellDetector detectionPhase = new SpellDetector(suggestionPhase,
				rbHandler);
		PreProcessor preProcessingPhase = new PreProcessor(detectionPhase);
		spellHandler = preProcessingPhase;

	}

	public void setReady(boolean isReady) {
		this.isReady = isReady;
	}

}
