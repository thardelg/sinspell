package aksharawekshakaya.spellchecker;

import aksharawekshakaya.spellengine.GenericSuggester;

public abstract class SuggestionHandler {
	protected GenericSuggester suggester;
	protected SuggestionHandler next;
	
	public SuggestionHandler(SuggestionHandler next) {
		this.next = next;
	}
	
	public void handle(SpellSuggester spellSuggester) {
		next.handle(spellSuggester);
	}

	public GenericSuggester getDetector() {
		return suggester;
	}

	public void setDetector(GenericSuggester detector) {
		this.suggester = detector;
	}

	public SuggestionHandler getNext() {
		return next;
	}

	public void setNext(SuggestionHandler next) {
		this.next = next;
	}
}
