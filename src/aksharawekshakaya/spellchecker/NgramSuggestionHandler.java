package aksharawekshakaya.spellchecker;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import aksharawekshakaya.spellengine.GenericSuggester;
import aksharawekshakaya.spellengine.NgramBasedSuggester;
import aksharawekshakaya.spellengine.PhoneticSuggester;
import aksharawekshakaya.spellengine.Suggestion;

public class NgramSuggestionHandler extends SuggestionHandler {

	protected GenericSuggester genSug;
	public NgramSuggestionHandler(SuggestionHandler next, PhoneticSuggester phoneSuggester, double threshold) {
		super(next);
		genSug = new NgramBasedSuggester(phoneSuggester, threshold);
	}
	
	@Override
	public void handle(SpellSuggester spellSuggester) {
		Set<String> errorSet = spellSuggester.getErrorSet();
		Map<String, Set<Suggestion>> suggestionsMap = spellSuggester.getSuggestionsMap();
		
		for(String misspelled : errorSet) {
			List<Suggestion> sugs = genSug.suggest(misspelled);
			if((sugs == null) || sugs.isEmpty()) {
				continue;
			} else {
				if(suggestionsMap.containsKey(misspelled)) {
					Set<Suggestion> existing = suggestionsMap.get(misspelled);
					existing.addAll(sugs);
					suggestionsMap.put(misspelled, existing);
				} else {
					Set<Suggestion> newSugs = new LinkedHashSet<Suggestion>(sugs);
					suggestionsMap.put(misspelled, newSugs);
				}
				
			}
		}
		spellSuggester.setSuggestionsMap(suggestionsMap);
		spellSuggester.setDone(true);
	}

}
