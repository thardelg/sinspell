package aksharawekshakaya.config;

import java.io.Serializable;

public interface Configurable extends Serializable {
	
	public boolean getProperty(String prop, boolean def); //given a property name and default value returns property value
	public String getProperty(String prop, String def); // if property name doesen't exist, add new property with default value
	public Long getProperty(String prop, Long def);
	public double getProperty(String prop, double def);
	
	public void setProperty(String prop, String val);
	public void setProperty(String prop, boolean val);
	public void setProperty(String prop, Long val);
	public void setProperty(String prop, double val);
}
