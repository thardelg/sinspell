package aksharawekshakaya.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;

public class PropertyConfig extends Configuration implements Configurable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3377028336275318112L;
	private Properties props;
	private static PropertyConfig currentConfigs = new PropertyConfig();
	private File f = new File("C:\\Documents\\APIIT Projects\\Level-3\\Semester - 2\\FYP\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\properties\\config.properties");
	
	private PropertyConfig() {
		props = new Properties();
		try {
			if(!f.exists()) {
				f.createNewFile();}
			FileInputStream in = new FileInputStream(f);
			props.load(in);
			if(props.isEmpty()) {
				loadDefaults();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static PropertyConfig getInstance() { 
		if(currentConfigs == null) {
			return new PropertyConfig();
		}
		return currentConfigs;
	}

	@Override
	public boolean getProperty(String key, boolean def) {
		if(!props.containsKey(key)) {
			props.setProperty(key, String.valueOf(def));
			save();
		}
		return new Boolean(props.getProperty(key)).booleanValue();
	}

	@Override
	public String getProperty(String key, String def) {
		if(!props.containsKey(key)) {
			props.setProperty(key, String.valueOf(def));
			save();
		}
		return props.getProperty(key);
	}

	@Override
	public Long getProperty(String key, Long def) {
		if(!props.containsKey(key)) {
			props.setProperty(key, String.valueOf(def));
			save();
		}
		return new Long(props.getProperty(key)).longValue();
	}

	@Override
	public double getProperty(String key, double def) {
		if(!props.containsKey(key)) {
			props.setProperty(key, String.valueOf(def));
			save();
		}
		return new Double(props.getProperty(key)).doubleValue();
	}

	@Override
	public void setProperty(String key, String val) {
		props.setProperty(key, String.valueOf(val));
		save();
	}

	@Override
	public void setProperty(String key, boolean val) {
		props.setProperty(key, String.valueOf(val));
		save();
	}

	@Override
	public void setProperty(String key, Long val) {
		props.setProperty(key, String.valueOf(val));
		save();
	}

	@Override
	public void setProperty(String key, double val) {
		props.setProperty(key, String.valueOf(val));
		save();
	}
	
	public void viewPropertyList() {
		props.list(System.out);
	}
	
	public void save() {
		FileOutputStream out;
		try {
			out = new FileOutputStream(f);
			props.store(out, "values edited");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	@Override
	public void loadDefaults() {
		Field[] defaultConfigs = new DefaultConfig().getClass().getFields();
		try {
		for(Field f : defaultConfigs) {
			String key = f.getName();
			String value = (String) f.get(this);
			props.setProperty(key, value);
			save();
		}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	

}
