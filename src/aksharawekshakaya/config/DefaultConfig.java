package aksharawekshakaya.config;

public class DefaultConfig {

	public static final String DEFAULT_ENCODING = "UTF-16";
	public static final String DEFAULT_NORMAL_FORM = "NFC_UTF16";
	public static final String EDGE_NGRAM_TOKEN = "S";
	public static final String DICTIONARY_PATH = "C:\\Documents\\APIIT Projects\\Level-3\\Semester - 2\\FYP\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\dic\\";
	public static final String NGRAM_PATH = "C:\\Documents\\APIIT Projects\\Level-3\\Semester - 2\\FYP\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\ngrams\\";
	public static final String PROPERTY_PATH = "C:\\Documents\\APIIT Projects\\Level-3\\Semester - 2\\FYP\\Design & Implementation\\Implementation\\Aksharawekshakaya\\resources\\properties\\";
	public static final String INDEX_PATH = "C:\\Documents\\APIIT Projects\\Level-3\\Semester - 2\\FYP\\Implementation\\Aksharawekshakaya\\resources\\ngrams\\indexes\\";
	public static final String DATA_PATH = "C:\\Documents\\APIIT Projects\\Level-3\\Semester - 2\\FYP\\Implementation\\Aksharawekshakaya\\resources\\ngrams\\data\\";
	public static final String DEFAULT_UTF = "UTF-16";
	public static final String DEFAULT_PHONETIC_SPAN = "4";
	public static final String DEFAULT_SOUNDEX_SPAN = "6";
	public static final String DEFAULT_ED_THRESHOLD = "2";
	public static final String DEFAULT_NGRAM_THRESHOLD = "2";
	public static final String SYLLABLE_UNIGRAM_TOTAL = "0";
	public static final String SYLLABLE_TRIGRAM_TOTAL = "0";
	public static final String SYLLABLE_BIGRAM_TOTAL = "0";
	public static final String WORD_UNIGRAM_TOTAL = "0";
	public static final String WORD_TRIGRAM_TOTAL = "0";
	public static final String WORD_BIGRAM_TOTAL = "0";
	public static final String SYLLABLE_BIGRAM_DIC = "syllableBiGrams.dic";
	public static final String SYLLABLE_TRIGRAM_DIC = "syllableTriGrams.dic";
	public static final String SYLLABLE_UNIGRAM_DIC = "syllableUniGrams.dic";
	public static final String WORD_UNIGRAM_DIC = "wordUniGrams.dic";
	public static final String LEXICON_DIC = "lexicon.dic";
	public static final String USER_DIC = "user.dic";
	public static final String PHONETIC_DIC = "phonetics.dic";
	public static final String PHONETIC_DIC_ABSL = "phonetics_absl.dic";

}
