package aksharawekshakaya.config;

public abstract class Configuration {
	
	public static final String DEFAULT_ENCODING = "DEFAULT_ENCODING";
	public static final String DEFAULT_NORMAL_FORM = "DEFAULT_NORMAL_FORM";
	public static final String EDGE_NGRAM_TOKEN = "EDGE_NGRAM_TOKEN";
	public static final String DICTIONARY_PATH = "DICTIONARY_PATH";
	public static final String NGRAM_PATH = "NGRAM_PATH";
	public static final String PROPERTY_PATH = "PROPERTY_PATH";
	public static final String INDEX_PATH = "INDEX_PATH";
	public static final String DATA_PATH = "DATA_PATH";
	public static final String DEFAULT_UTF = "DEFAULT_UTF";
	public static final String DEFAULT_PHONETIC_SPAN = "DEFAULT_PHONETIC_SPAN";
	public static final String DEFAULT_SOUNDEX_SPAN = "DEFAULT_SOUNDEX_SPAN";
	public static final String DEFAULT_ED_THRESHOLD = "DEFAULT_ED_THRESHOLD";
	public static final String DEFAULT_NGRAM_THRESHOLD = "DEFAULT_NGRAM_THRESHOLD";
	public static final String SYLLABLE_UNIGRAM_TOTAL = "SYLLABLE_UNIGRAM_TOTAL";
	public static final String SYLLABLE_TRIGRAM_TOTAL = "SYLLABLE_TRIGRAM_TOTAL";
	public static final String SYLLABLE_BIGRAM_TOTAL = "SYLLABLE_BIGRAM_TOTAL";
	public static final String WORD_UNIGRAM_TOTAL = "WORD_UNIGRAM_TOTAL";
	public static final String WORD_TRIGRAM_TOTAL = "WORD_TRIGRAM_TOTAL";
	public static final String WORD_BIGRAM_TOTAL = "WORD_BIGRAM_TOTAL";
	public static final String SYLLABLE_BIGRAM_DIC = "SYLLABLE_BIGRAM_DIC";
	public static final String SYLLABLE_TRIGRAM_DIC = "SYLLABLE_TRIGRAM_DIC";
	public static final String SYLLABLE_UNIGRAM_DIC = "SYLLABLE_UNIGRAM_DIC";
	public static final String WORD_UNIGRAM_DIC = "WORD_UNIGRAM_DIC";
	public static final String LEXICON_DIC = "LEXICON_DIC";
	public static final String USER_DIC = "USER_DIC";
	public static final String PHONETIC_DIC = "PHONETIC_DIC";
	public static final String PHONETIC_DIC_ABSL = "PHONETIC_DIC_ABSL";
	
	public abstract void loadDefaults();
}
