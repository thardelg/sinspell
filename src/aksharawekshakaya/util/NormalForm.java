package aksharawekshakaya.util;
public enum NormalForm {
	
	NFC,
	NFD,
	NFC_UTF8,
	NFC_UTF16;
}
