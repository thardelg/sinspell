package aksharawekshakaya.util;

import com.ibm.icu.text.Normalizer;
import aksharawekshakaya.config.Configuration;
import aksharawekshakaya.config.PropertyConfig;

public class BaseNormalizer implements Normalizable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6674777087102757315L;
	private final PropertyConfig config;
	protected NormalForm CURRENT_NORMAL_FORM;
	protected String soruce;
	protected Convertible converter;

	public BaseNormalizer(String source) {
		this.soruce = source;
		config = PropertyConfig.getInstance();
		CURRENT_NORMAL_FORM = NormalForm.valueOf(config.getProperty(Configuration.DEFAULT_NORMAL_FORM, "NFC_UTF16"));
		setConverter(CURRENT_NORMAL_FORM);
	}
	public BaseNormalizer(String source, NormalForm normalForm) {
		this.soruce = source;
		config = PropertyConfig.getInstance();
		CURRENT_NORMAL_FORM = normalForm;
		setConverter(normalForm);
	}
	public String getSourceString(){
		return this.soruce;
	}
	public void setSourceString(String source){
		this.soruce = source;
	}
	@Override
	public String normalize(String src, NormalForm normalForm) {
		return getConverter(normalForm).convert(src);
	}
	@Override
	public boolean isNormalized(String src, NormalForm normalForm) {
		return Normalizer.isNormalized(src,Normalizer.NFC, 0);
	}
	@Override
	public boolean isNormalized(NormalForm normalForm) {
		return Normalizer.isNormalized(this.soruce,Normalizer.NFC, 0);
	}
	@Override
	public NormalForm currentForm() {
		return this.CURRENT_NORMAL_FORM;
	}
	@Override
	public String normalize() {
		return converter.convert(this);
	}
	@Override
	public String normalize(String src) {
		return converter.convert(src);
	}
	public NormalForm getCurrentNormalForm() {
		return CURRENT_NORMAL_FORM;
	}
	public Convertible getConverter(NormalForm normalForm) {
		Convertible con;
		switch (normalForm) {
		case NFC : 
			con = new NFCConverter(); break;
		case NFD : 
			con = new NFDConverter(); break;
		case NFC_UTF16 : 
			 UTFConverter utfCon16  = new UTFConverter(); 
			 utfCon16.setCurrentUTF("UTF-16");
			 con = utfCon16;break;
		case NFC_UTF8  : 
			 UTFConverter utfCon8  = new UTFConverter(); 
			 utfCon8.setCurrentUTF("UTF-8");
			 con = utfCon8;break;
		default  : 
			con = new NFCConverter();
		}
		return con;
	}
	
	public String convertToUtf16(String source){
		return getConverter(NormalForm.NFC_UTF16).convert(source);
	}
	
	public void setConverter(NormalForm normalForm){
		this.converter = getConverter(normalForm);
	}
	
}
