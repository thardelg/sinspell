package aksharawekshakaya.util;
import com.ibm.icu.text.Normalizer;

public class NFCConverter implements Convertible {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5539646177958384535L;

	@Override
	public String convert(BaseNormalizer normalizer) {
		String source = normalizer.getSourceString();
		String target = Normalizer.normalize(source,Normalizer.NFC);
		normalizer.setSourceString(target);
		return target;
	}

	@Override
	public String convert(String source) {
		return Normalizer.normalize(source,Normalizer.NFC);
	}

}
