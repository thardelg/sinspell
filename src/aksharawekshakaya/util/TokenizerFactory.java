package aksharawekshakaya.util;

import java.io.File;

public class TokenizerFactory {
	
	public static StringTokenizer getStringTokenizer(String context, TokenType type) {
		switch (type) {
		case WORD: 
			return new StringTokenizer(context, new WordToken(0, " "));
		case SENTANCE: 
			return new StringTokenizer(context, new SentanceToken(0, " "));
		case SYLLABLE:
			return new StringTokenizer(context, new SyllableToken(0, " "));
		default:
			return new StringTokenizer(context, new WordToken(0, " "));
		}
	}
	
	public static FileTokenizer getFileTokenizer(File context, TokenType type) {
		switch (type) {
		case WORD: 
			return new FileTokenizer(context, new WordToken(0, " "));
		case SENTANCE: 
			return new FileTokenizer(context, new SentanceToken(0, " "));
		case SYLLABLE:
			return new FileTokenizer(context, new WordToken(0, " "));
		default:
			return new FileTokenizer(context, new WordToken(0, " "));
		}
	}
	
	public static FileTokenizer getNFileTokenizer(File context, TokenType type) {
		switch (type) {
		case WORD: 
			return new FileTokenizer(context, new SentanceToken(0, " "));
		case SENTANCE: 
			return new FileTokenizer(context, new SentanceToken(0, " "));
		case SYLLABLE:
			return new FileTokenizer(context, new WordToken(0, " "));
		default:
			return new FileTokenizer(context, new WordToken(0, " "));
		}
	}
}
