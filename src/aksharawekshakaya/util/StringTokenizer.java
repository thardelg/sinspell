package aksharawekshakaya.util;

import com.ibm.icu.text.BreakIterator;
import com.ibm.icu.util.ULocale;

public class StringTokenizer implements Tokenizable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7266763229800341026L;
	protected Token typeCheck;
	protected String context;
	protected Token current;
	protected Token next;
	protected boolean isSentanceStart;
	protected transient BreakIterator iterator;
	protected transient ULocale locale;
	
	public StringTokenizer(String context, Token token){
		typeCheck = token;
		locale = new ULocale("si");
		switch (token.getType()) {
		case WORD:
			iterator = BreakIterator.getWordInstance(locale);
			break;
		case SENTANCE:
			iterator = BreakIterator.getSentenceInstance(locale);
			break;
		case SYLLABLE:
			iterator = BreakIterator.getCharacterInstance(locale);
			break;
		default:
			iterator = BreakIterator.getWordInstance(locale);
			break;
		}
		this.context = context.trim();
		init(typeCheck);
	}
	@Override
	public boolean hasNext() {
		return next != null;
	}

	@Override
	public Token next() {
		current.replace(next);
		setIterator();
		int start = iterator.current(); 
		int end   = iterator.next();
		if(end != BreakIterator.DONE){
			next.setValue(context.substring(start, end));
		} else {
			next = null;
		}
		
		if(!current.acceptable() && (next!= null)){
			next();
		} else if(current.acceptable()) { //this condition needed?
			return current;
		}
		
		return current;
	}

	@Override
	public void remove() {
		
	}

	@Override
	public String getContext() {
		return context;
	}

	@Override
	public void setContext(String context) {
		this.context = context;
		init(typeCheck);
		
	}

	@Override
	public Token getCurrent() {
		return current;
	}

	@Override
	public void replace(Token newToken) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean atSentanceStart() {
		return isSentanceStart;
	}
	
	private void init(Token token){
		current = token.createToken(0, "");
		next = token.createToken(0,  "");
		isSentanceStart = true;
		iterator.setText(context);
	}
	
	public void setIterator() {
		int curr = iterator.current();
		if(curr == current.getStartIndex()) {
			isSentanceStart = true;
		} else {
			isSentanceStart = false;
			if(current.getEndIndex() > curr) {
				iterator.next();
			}
		}
	}
	
	@Override
	public String toString() {
		return context;
	}

}

//if((context.charAt(start) == ' ') && (end != BreakIterator.DONE))
//{
//	start = iterator.next();
//	end   = iterator.next();
//}
