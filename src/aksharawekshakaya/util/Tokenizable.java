package aksharawekshakaya.util;
import java.io.Serializable;
import java.util.Iterator;

public interface Tokenizable  extends Iterator<Token> , Serializable{
	public String getContext();
	public void setContext(String context);
	public Token getCurrent();
	public void replace(Token newToken);
	public boolean atSentanceStart();
}
