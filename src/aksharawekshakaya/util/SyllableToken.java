package aksharawekshakaya.util;

public class SyllableToken extends AbstractToken {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3541191699434047902L;

	public SyllableToken(int start, String syllalbe){
		setValue(syllalbe);
		setStartIndex(start);
	}
	
	public SyllableToken(SyllableToken syllalbe){
		replace(syllalbe);
	}
	
	@Override
	public boolean isSinhala() {
		String word = super.value();
		return TextUtils.isSinhala(word);
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return (SyllableToken)super.clone();
	}

	@Override
	public TokenType getType() {
		return TokenType.SYLLABLE;
	}

	@Override
	public Token createToken(int start, String val) {
		return new SyllableToken(start, val);
	}
	
	@Override
	public String toString() {
		return value();
	}

	@Override
	public boolean acceptable() {
		return !TextUtils.containsWhiteSpace(value()); 
	}

}
