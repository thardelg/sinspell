package aksharawekshakaya.util;

import java.io.Serializable;

public interface Normalizable extends Serializable{
	public String normalize(); //normalize wrappee
	public String normalize(String source);//normalize external string to current normal form
	public String normalize(String source, NormalForm normalForm); //normalize external string to the default form
	public boolean isNormalized(String source, NormalForm normalForm); //is the
	public boolean isNormalized(NormalForm normalForm); //is the current string normalized
	public NormalForm currentForm();
}
