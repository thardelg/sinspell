package aksharawekshakaya.util;

import java.io.Serializable;

public interface Token extends Serializable{
	public int getStartIndex();
	public int getEndIndex();
	public String value();
	public void setStartIndex(int start);
	public void setEndIndex();
	public void setValue(String value);
	public void replace(Token newToken);
	public boolean acceptable();
	public TokenType getType();
	public Token createToken(int start, String val); //returns a new token of its type
	public boolean isSinhala(); //whether token contains English only
}
