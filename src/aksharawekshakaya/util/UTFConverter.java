package aksharawekshakaya.util;
import java.io.UnsupportedEncodingException;
import aksharawekshakaya.config.Configuration;
import aksharawekshakaya.config.PropertyConfig;


public class UTFConverter implements Convertible {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6936731340052362516L;
	PropertyConfig config;
	public static String currentUTF = "UTF-16";
	public UTFConverter() {
		config = PropertyConfig.getInstance();
		currentUTF = config.getProperty(Configuration.DEFAULT_UTF,"UTF-16");
	}
	@Override
	public String convert(BaseNormalizer normalizer) {
		String source = normalizer.getSourceString();
		String target = getTarget(source);
		normalizer.setSourceString(target);
		return target;
	}
	@Override
	public String convert(String source) {;
		return getTarget(source);
	}
	private String getTarget(String source) {
		byte[] target;
		Convertible nfc = new NFCConverter();
		try {
			target = source.getBytes(currentUTF); //converts source string to a byte array
			return nfc.convert(new String(target, currentUTF)); //convert byte array to UTF-16
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return source;
		}
	}
	public String getCurrentUTF() {
		return currentUTF;
	}
	public void setCurrentUTF(String currentUTF) {
		UTFConverter.currentUTF = currentUTF;
	}
	

}
