package aksharawekshakaya.util;

public class SentanceToken extends AbstractToken {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2997646690140060658L;

	public SentanceToken(int start, String sentance){
		setValue(sentance);
		setStartIndex(start);
	}
	
	public SentanceToken(SentanceToken sentance){
		replace(sentance);
	}
	
	@Override
	public boolean isSinhala() {
		String word = super.value();
		return TextUtils.isSinhala(word);
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return (SentanceToken)super.clone();
	}

	@Override
	public TokenType getType() {
		return TokenType.SENTANCE;
	}

	@Override
	public Token createToken(int start, String val) {
		return new SentanceToken(start, val);
	}
	
	@Override
	public String toString() {
		return value();
	}

	@Override
	public boolean acceptable() {
		return true; 
	}

}
