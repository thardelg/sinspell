package aksharawekshakaya.util;

public class Normalizer extends BaseNormalizer {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4381036108383275785L;
	//rendering for ISKOOLA POTHA Sinhala Unicode Font
	public static final String ZWJ = "\u200D";
	public static final String YANSAYA = "\u0DCA\u200D\u0DBA";
	public static final String YANSAYA_REM = "\u0DCA\u0DBA";
	public static final String RAKARANSAYA = "\u0DCA\u200D\u0DBB";
	public static final String RAKARANSAYA_REM = "\u0DCA\u0DBB";
	public static final String REPAYA = ""; // not considered
	public static final String KSHA = "\u0D9A\u0DCA\u200D\u0DC2";
	public static final String KSHA_REM = "\u0D9A\u0DCA\u0DC2";
	//public static final String KWA = "\u0D9A\u0DCA\u200D\u0DC0";
	//public static final String KWA_REM = "\u0D9A\u0DCA\u0DC0";
	public static final String NDA = "\u0DB1\u0DCA\u200D\u0DAF";
	public static final String NDA_REM = "\u0DB1\u0DCA\u0DAF";
	public static final String NDHA = "\u0DB1\u0DCA\u200D\u0DB0";
	public static final String NDHA_REM = "\u0DB1\u0DCA\u200D\u0DB0";
	//public static final String TWA = "\u0DAD\u0DCA\u200D\u0DC0";
	//public static final String TWA_REM = "\u0DAD\u0DCA\u0DC0";
	public static final String TTHA = "\u0DAD\u0DCA\u200D\u0DAE";
	public static final String TTHA_REM = "\u0DAD\u0DCA\u0DAE";
	public static final String WWA = "\u0DB3\u0DCA\u200D\u0DC0";
	public static final String WWA_REM = "\u0DB3\u0DCA\u0DC0";
	public static final String NDTHA = "\u0DB3\u0DCA\u200D\u0DA8";
	public static final String NDTHA_REM = "\u0DB3\u0DCA\u0DA8";
	public static final String NNDA = "\u0DB3\u0DCA\u200D\u0DB0";
	public static final String NNDA_REM = "\u0DB3\u0DCA\u0DB0";
	public static final String MMA = "\u0DB8\u200D\u0DCA\u0DB8";
	public static final String MMA_REM = "\u0DB8\u200D\u0DCA\u0DB8";
	public static final String SP_CASE = "\u0DDA\u200D\u0DCA\u200D\u0DBB";
	public static final String SP_CASE_REM = "\u0DDA\u0DCA\u0DBB";
	
	public static final String[] conjunctSet = {
		YANSAYA, RAKARANSAYA, KSHA, /*KWA,*/ NDA, NDHA, /*TWA,*/ TTHA, 
			WWA, NDTHA, NNDA, MMA
		};
	
	public static final String[] conjunctRemSet = {
		 YANSAYA_REM, RAKARANSAYA_REM, KSHA_REM, /*KWA_REM,*/ NDA_REM, 
			NDHA_REM, /*TWA_REM,*/ TTHA_REM, WWA_REM, NDTHA_REM, NNDA_REM, 
			MMA_REM
		};
	
	
	@Override
	public String normalize(String src) {
		src = super.normalize(src);
		return stripZJW(src);
	};
	public Normalizer(String src) {
		super(src);
	}
	
	public Normalizer(String src, NormalForm normalForm) {
		super(src,normalForm);
	}
	
	public static String stripZJW(String src){
		return src.replace(ZWJ, "");
	}
	
	public static String replaceZJW(String src){
		String target = stripZJW(src);
		for (int i = 0; i < conjunctRemSet.length; i++) {
			target = target.replace(conjunctRemSet[i], conjunctSet[i]);
		}
		return target;
	}
	
	public String getSinhala(String src){
		TextUtils.filterString("cs","es");
		return null;
	}
	
	
}
