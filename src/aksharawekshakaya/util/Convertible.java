package aksharawekshakaya.util;

import java.io.Serializable;

public interface Convertible extends Serializable {
	public String convert(BaseNormalizer normalizer);
	public String convert(String source);
}
