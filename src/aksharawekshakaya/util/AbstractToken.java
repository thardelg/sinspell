package aksharawekshakaya.util;

public abstract class AbstractToken implements Token, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8990579182084878869L;
	protected int start; // private coz work might change values without knowing
	protected int end;
	protected String value; 
	
	@Override
	public int getStartIndex() {
		return start;
	}

	@Override
	public int getEndIndex() {
		return end;
	}

	@Override
	public String value() {
		return value;
	}

	@Override
	public void setStartIndex(int start) {
		this.start = start;
		setEndIndex();
	}

	@Override
	public void setEndIndex() {
		end = start + value.length();
	}

	@Override
	public void setValue(String value) {
		this.value = value;
		setEndIndex();
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	@Override
	public void replace(Token newToken) {
		start = newToken.getStartIndex();
		value  = newToken.value(); 
		setEndIndex();
	}
	
	@Override
	public String toString() {
		return value;
	}

	public abstract boolean isSinhala();
	
	public abstract boolean acceptable();
}
