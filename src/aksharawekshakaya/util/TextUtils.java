package aksharawekshakaya.util;

public class TextUtils {
	
	public final static char[] sinhalaInversionSet = {
		'\u0D80' , '\u0E00' //represent sinhala unicode range
	};
	
	public final static String sinhalaSet_NFD =
		"\u0D82\u0D83\u0D85\u0D86\u0D87\u0D88\u0D89\u0D8A\u0D8B\u0D8C\u0D8D\u0D91\u0D92\u0D93\u0D94 " +
		"\u0D95\u0D96\u0D9A\u0D9B\u0D9C\u0D9D\u0D9E\u0D9F\u0DA0\u0DA1\u0DA2\u0DA3\u0DA4\u0DA5\u0DA7"  +
		"\u0DA8\u0DA9\u0DAA\u0DAB\u0DAC\u0DAD\u0DAE\u0DAF\u0DB0\u0DB1\u0DB3\u0DB4\u0DB5\u0DB6\u0DB7"  +
		"\u0DB8\u0DB9\u0DBA\u0DBB\u0DBD\u0DC0\u0DC1\u0DC2\u0DC3\u0DC4\u0DC5\u0DC6\u0DCA\u0DCF\u0DD0"  +
		"\u0DD1\u0DD2\u0DD3\u0DD4\u0DD6\u0DD8\u0DD9\u0DDA\u0DDB\u0DDC\u0DDD\u0DDE\u0DDF\u0DF2";
	
	public final static char[] sinhalaCharset_NFD = { 
		'\u0D82', '\u0D83', '\u0D85', '\u0D86', '\u0D87', '\u0D88', '\u0D89', '\u0D8A', '\u0D8B',
		'\u0D8C', '\u0D8D', '\u0D91', '\u0D92', '\u0D93', '\u0D94', '\u0D95', '\u0D96', '\u0D9A',
		'\u0D9B', '\u0D9C', '\u0D9D', '\u0D9E', '\u0D9F', '\u0DA0', '\u0DA1', '\u0DA2', '\u0DA3',
		'\u0DA4', '\u0DA5', '\u0DA7', '\u0DA8', '\u0DA9', '\u0DAA', '\u0DAB', '\u0DAC', '\u0DAD',
		'\u0DAE', '\u0DAF', '\u0DB0', '\u0DB1', '\u0DB3', '\u0DB4', '\u0DB5', '\u0DB6', '\u0DB7',
		'\u0DB8', '\u0DB9', '\u0DBA', '\u0DBB', '\u0DBD', '\u0DC0', '\u0DC1', '\u0DC2', '\u0DC3',
		'\u0DC4', '\u0DC5', '\u0DC6', '\u0DCA', '\u0DCF', '\u0DD0', '\u0DD1', '\u0DD2', '\u0DD3',
		'\u0DD4', '\u0DD6', '\u0DD8', '\u0DD9', '\u0DDA', '\u0DDB', '\u0DDC', '\u0DDD', '\u0DDE',
		'\u0DDF', '\u0DF2'
	};
	
	public final static char[] whiteSpaceCharset = {
             '\u0009' // CHARACTER TABULATION
            , '\u000B' // LINE TABULATION
            , '\u000C' // FORM FEED (FF)
            , '\u0020' // SPACE
            , '\u0085' // NEXT LINE (NEL) 
            , '\u00A0' // NO-BREAK SPACE
            , '\u1680' // OGHAM SPACE MARK
            , '\u180E' // MONGOLIAN VOWEL SEPARATOR
            , '\u2000' // EN QUAD 
            , '\u2001' // EM QUAD 
            , '\u2002' // EN SPACE
            , '\u2003' // EM SPACE
            , '\u2004' // THREE-PER-EM SPACE
            , '\u2005' // FOUR-PER-EM SPACE
            , '\u2006' // SIX-PER-EM SPACE
            , '\u2007' // FIGURE SPACE
            , '\u2008' // PUNCTUATION SPACE
            , '\u2009' // THIN SPACE
            , '\u200A' // HAIR SPACE
            , '\u2028' // LINE SEPARATOR
            , '\u2029' // PARAGRAPH SEPARATOR
            , '\u202F' // NARROW NO-BREAK SPACE
            , '\u205F' // MEDIUM MATHEMATICAL SPACE
            , '\u3000' // IDEOGRAPHIC SPACE 
            }; 
	
	public static boolean characterInSet(char c, char[] set) {
		int low = 0;
		int high = set.length;
		while (low < high) {
			int mid = (low + high) / 2;
			if (c >= set[mid]) //if in a higher Unicode range
				low = mid + 1;
			else if (c < set[mid])
				high = mid;
		}
		int pos = high - 1;
		return (pos & 1) == 0;
	}
	
	public static String filterString(String source, String filterPattern){
		return source.replaceAll(filterPattern, "");
	}
	
	public static String getSinhalaOnly(String source){
		StringBuilder sb = new StringBuilder("");
		for(char c : source.toCharArray()) {
			if(isSinhala(c) || Character.isSpaceChar(c))
				sb.append(c);
		}
		return sb.toString();
	}
	
	public static boolean isSinhala(char c) { //check only for Sinhala, no digits or special chars
		return characterInSet(c, sinhalaInversionSet);
	}
	
	public static boolean isSinhala(String source) {
		String s= filterString(source, " "); //avoid white space
		boolean flag = true;
		for(char c : s.toCharArray()) {
			flag = flag && isSinhala(c);
		}
		return flag;
	}
	
	public static boolean containsWhiteSpace(String s){
		boolean flag = true;
		for(char c : s.toCharArray()){
			flag = (flag && Character.isWhitespace(c));
		}
		return flag;
	}
	
	public static double getDouble(String source) {
		String[] set = source.split("\t");
		return Double.parseDouble(set[set.length-1].trim());
	}
	
	public static long getLong(String source) {
		String[] set = source.split("\t");
		return Long.parseLong(set[set.length-1].trim());
	}
	
	public static long getInt(String source) {
		return (int)getLong(source);
	}
	
	public static float getFloat(String source) {
		return (float)getDouble(source);
	}
	
	public static String getStringKey(String source) {
		String[] set = source.split("\t");
		return set[0].trim();
	}
	
	public static boolean isDigit(String source) {
		boolean flag = true;
		for(char c : source.toCharArray()) {
			flag = flag && Character.isDigit(c);
		}
		return flag;
	}
	
	public static String getDigits(String source) {
		StringBuilder sb = new StringBuilder();
		for (char c : source.toCharArray()) {
			if (Character.isDigit(c))
				sb.append(c);
		}
		return sb.toString();
	}
	
}
