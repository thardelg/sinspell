package aksharawekshakaya.util;
import com.ibm.icu.text.Normalizer;

public class NFDConverter implements Convertible {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6213300293666809157L;

	@Override
	public String convert(BaseNormalizer normalizer) {
		String source = normalizer.getSourceString();
		String target = Normalizer.normalize(source,Normalizer.NFD);
		normalizer.setSourceString(target);
		return target;
	}

	@Override
	public String convert(String source) {
		return Normalizer.normalize(source,Normalizer.NFD);
	}

}
