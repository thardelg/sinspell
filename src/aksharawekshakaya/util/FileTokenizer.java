package aksharawekshakaya.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import aksharawekshakaya.dictionaries.FileManager;

public class FileTokenizer implements Tokenizable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1118618245761732285L;
	protected File contentFile;
	protected StringTokenizer tokenizer;
	protected FileManager fm;
	protected StringBuilder sb;

	public FileTokenizer(File contentFile, Token token){
		this.contentFile = FileManager.getFile(contentFile);
		fm = FileManager.getInstance();
		StringBuilder sb = new StringBuilder();
		BufferedReader in = FileManager.getReader(contentFile);
		String s;
		try {
			s = in.readLine();
			while(s!=null) {
				sb.append(s.trim()).append(" ");
				s = in.readLine();  
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		tokenizer = new StringTokenizer(sb.toString(), token);
	}
	@Override
	public boolean hasNext() {
		return tokenizer.hasNext();
	}

	@Override
	public Token next() {
		return tokenizer.next();
	}

	@Override
	public void remove() {
		tokenizer.remove();
		
	}

	@Override
	public String getContext() {
		return tokenizer.getContext();
	}

	@Override
	public void setContext(String context) {
		tokenizer.setContext(context);
	}

	@Override
	public Token getCurrent() {
		return tokenizer.getCurrent();
	}

	@Override
	public void replace(Token newToken) {
		tokenizer.replace(newToken);
	}

	@Override
	public boolean atSentanceStart() {
		return tokenizer.atSentanceStart();
	}
	

}
