package aksharawekshakaya.util;

public class WordToken extends AbstractToken {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7753283059638673712L;

	public WordToken(int start, String word){
		setValue(word);
		setStartIndex(start);
	}
	
	public WordToken(WordToken word){
		replace(word);
	}
	
	@Override
	public boolean isSinhala() {
		String word = super.value();
		return TextUtils.isSinhala(word);
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return (WordToken)super.clone();
	}

	@Override
	public TokenType getType() {
		return TokenType.WORD;
	}

	@Override
	public Token createToken(int start, String val) {
		return new WordToken(start, val);
	}
	
	@Override
	public String toString() {
		return value();
	}

	@Override
	public boolean acceptable() {
		return !TextUtils.containsWhiteSpace(value()); 
	}

}
