package aksharawekshakaya.spellengine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public abstract class GenericSuggester implements Suggestable {
	
	protected List<Suggestion> suggestions;
	protected String misspelled;
	protected boolean isSuggested;
	
	public GenericSuggester() {
		suggestions = new ArrayList<Suggestion>();
		misspelled = null;
		isSuggested = false;
	}

	@Override
	public abstract List<Suggestion> suggest(String misspelled);
	
	@Override
	public boolean isSuggested() {
		return isSuggested;
	}
	
	@Override
	public String getMispelled() {
		return misspelled;
	}

	@Override
	public double getScore(Suggestion s) {
		return s.getScore();
	}
	
	@Override
	public double getMinScore() {
		sortSuggestions();
		return suggestions.get(0).getScore();
	}
	
	@Override
	public double getMaxScore() {
		sortSuggestions();
		return suggestions.get(suggestions.size()-1).getScore();
	}
	
	protected void sortSuggestions() {
		Collections.sort(suggestions);
	}
	
	protected void getSortedUnique() {
		sortSuggestions();
		Set<Suggestion> sugList = new LinkedHashSet<Suggestion>(suggestions);
		suggestions.clear();
		suggestions.addAll(sugList);
	}
	
	@Override
	public List<Suggestion> getCurrentList() {
		return suggestions;
	}

}
