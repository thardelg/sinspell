package aksharawekshakaya.spellengine;

import java.io.Serializable;

public abstract class PhoneticTransformator implements Transformable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7876839530475542345L;
	protected String word;
	protected String phoneticHash;
	
	public PhoneticTransformator(String word) {
		this.word = word;
	}
	@Override
	public abstract String transform(String word);
	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
		phoneticHash = transform(word);
	}
	public String getPhoneticHash() {
		return phoneticHash;
	}

}
