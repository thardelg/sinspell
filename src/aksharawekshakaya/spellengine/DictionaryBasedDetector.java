package aksharawekshakaya.spellengine;

import aksharawekshakaya.dictionaries.Dictionary;

public class DictionaryBasedDetector extends GenericDetector {
	protected Dictionary dictionary;
	public DictionaryBasedDetector(Dictionary dictionary) {
		this.dictionary = dictionary;
	}
	@Override
	public boolean detect(String word) {
		rate = 1.0;//100% accurate detection
		return dictionary.contains(word);
	}

	@Override
	public double getRate() {
		return rate;
	}

	@Override
	public Suggestable getNearestSuggestor() {
		// TODO SET ALL NEAREST SUGGESTERS
		return null;
	}

}
