package aksharawekshakaya.spellengine;

public abstract class GenericDetector implements Detectable {
	protected double rate;
	public abstract Suggestable getNearestSuggestor();
}
