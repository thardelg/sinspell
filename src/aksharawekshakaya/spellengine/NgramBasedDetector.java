package aksharawekshakaya.spellengine;

import aksharawekshakaya.dictionaries.KeyBasedDictionary;
import aksharawekshakaya.ngram.NgramType;
import aksharawekshakaya.ngram.StringNgramTokenizer;

public class NgramBasedDetector extends GenericDetector {
	protected KeyBasedDictionary ngramDictionary;
	protected NgramType gramType;
	protected StringNgramTokenizer tokenizer;
	protected double threshold;
	protected double offset;
	
	public NgramBasedDetector(KeyBasedDictionary ngramDictionary, NgramType type, double threshold, double offset) {
		this.ngramDictionary = ngramDictionary;
		this.gramType = type;
		this.threshold = threshold;
		this.offset = offset;
	}
	@Override
	public boolean detect(String word) {//true id a valid word
		tokenizer = new StringNgramTokenizer(word, gramType);
		double totalProb = 1;
		for(String s : tokenizer.getTokenSet()) {
			totalProb *= (ngramDictionary.getValue(s)*offset);
		}
		
		rate = totalProb;
		//System.out.println("Total Probability for '" + word + "' :" + totalProb);
		if(rate < threshold) { // FIXME : get this to propfile
			return false;
		} else {
			return true;
		}
	}

	@Override
	public double getRate() {
		return rate;
	}

	@Override
	public Suggestable getNearestSuggestor() {
		// TODO Auto-generated method stub
		return null;
	}

}
