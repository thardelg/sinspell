package aksharawekshakaya.spellengine;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import aksharawekshakaya.ngram.NgramType;
import aksharawekshakaya.ngram.StringNgramTokenizer;

public class NgramBasedSuggester extends GenericSuggester{
	//REFER PAPER
	protected PhoneticSuggester phoneSuggester;
	protected double threshold;
	public NgramBasedSuggester(PhoneticSuggester phoneSuggester, double threshold) {
		this.phoneSuggester = phoneSuggester;
		this.threshold = threshold;
	}
	@Override
	public List<Suggestion> suggest(String misspelled) {
		isSuggested = false;
		suggestions.clear();
		List<Suggestion> suggs = phoneSuggester.suggest(misspelled);
		if(suggs.isEmpty() || suggs == null) {
			return suggestions;
		} else {
			for(Suggestion s : suggs) {
				double score = getScore(s.getSuggestion(), misspelled); //FIXME add edit distance score here as well
				if(score >= threshold) {
	 				suggestions.add(new Suggestion(misspelled, s.getSuggestion(), score));
				}
			}
			isSuggested = true;
		}
		
		return suggestions;
	}
	
	public static double getScore(String w1, String w2){
		
		if(w1==null || w2 == null) {
			return 0;
		}
		
		if(w1.isEmpty() || w2.isEmpty()) {
			return 0;
		}
		
//		if(!TextUtils.isSinhala(w1) ||!TextUtils.isSinhala(w2) ) {
//			return 0;
//		}
		
		//System.out.print(w1 +" : "+ w2 + " Sizes -> ");
		
		
		if(w1.length() > w2.length()) {
			String temp = w2;
			w2 = w1;
			w1 = temp;
		}
		
		StringNgramTokenizer tokenizer;
		double score = 0.0;
		
		tokenizer = new StringNgramTokenizer(w1, NgramType.SYLLABLE_BI);
		List<String> w1GramSet = new ArrayList<String>(tokenizer.getTokenSet());
		
		tokenizer = new StringNgramTokenizer(w2, NgramType.SYLLABLE_BI);
		List<String> w2GramSet = new ArrayList<String>(tokenizer.getTokenSet());
		
		tokenizer = new StringNgramTokenizer(w1, NgramType.SYLLABLE_UNI);
		List<String> w1UniSet = new ArrayList<String>(tokenizer.getTokenSet());
		
		tokenizer = new StringNgramTokenizer(w2, NgramType.SYLLABLE_UNI);
		List<String> w2UniSet = new ArrayList<String>(tokenizer.getTokenSet());
		
		score += compare(w1UniSet.get(0), w2UniSet.get(0));
		score += compare(w1UniSet.get(w1UniSet.size()-1), w2UniSet.get(w2UniSet.size()-1));
		
//		System.out.print(w1GramSet.size() +" : "+ w1GramSet.size());
//		System.out.println();
		if(w1GramSet.size() < w2GramSet.size()) {
			for(int i=1; i<(w1GramSet.size()-1); i++) {
				if(i==1) {
					score += compare(w1GramSet.get(i), w2GramSet.get(i));
					score += compare(w1GramSet.get(i), w2GramSet.get(i+1));
				}else if(i==(w1GramSet.size()-2)) {
					score += compare(w1GramSet.get(i), w2GramSet.get(i));
					score += compare(w1GramSet.get(i), w2GramSet.get(i-1));
				}else {
					score += compare(w1GramSet.get(i), w2GramSet.get(i));
					score += compare(w1GramSet.get(i), w2GramSet.get(i-1));
					score += compare(w1GramSet.get(i), w2GramSet.get(i+1));
				}
			}
		} else {
			for(int i=1; i<(w2GramSet.size()-1); i++) {
				if(i==1) {
					score += compare(w2GramSet.get(i), w1GramSet.get(i));
					score += compare(w2GramSet.get(i), w1GramSet.get(i+1));
				}else if(i==(w2GramSet.size()-2)) {
					score += compare(w2GramSet.get(i), w1GramSet.get(i));
					score += compare(w2GramSet.get(i), w1GramSet.get(i-1));
				}else {
					score += compare(w2GramSet.get(i), w1GramSet.get(i));
					score += compare(w2GramSet.get(i), w1GramSet.get(i-1));
					score += compare(w2GramSet.get(i), w1GramSet.get(i+1));
				}
			}
		}
		
		
		Set<String> w1S = new HashSet<String>(w1GramSet);
		Set<String> w2S = new HashSet<String>(w2GramSet);
		w1S.addAll(w2S);
		score /= (double)w1S.size();
		return score;
	}
	
	public static int compare(String n1, String n2) {
		if(n1.equals(n2)) return 1;
		else return 0;
	}
}
