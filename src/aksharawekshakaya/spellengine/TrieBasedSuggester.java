package aksharawekshakaya.spellengine;

import java.util.List;

import aksharawekshakaya.dictionaries.TrieBasedDictionary;

public class TrieBasedSuggester extends GenericSuggester {
	protected TrieBasedDictionary dictionary;
	protected int distance;
	public TrieBasedSuggester(TrieBasedDictionary dictionary, int distance) {
		this.dictionary = dictionary;
		this.distance = distance;
	}
	@Override
	public List<Suggestion> suggest(String misspelled) {
		isSuggested = false;
		suggestions.clear();
		List<String> suggs = dictionary.similarSet(misspelled, distance);
		if(suggs.isEmpty() || suggs == null) {
			return suggestions;
		} else {
			for(String s : suggs) {
				int score = 1; //FIXME add edit distance score here as well
				suggestions.add(new Suggestion(misspelled, s, score));
			}
			isSuggested = true;
		}
		return suggestions;
	}

}
