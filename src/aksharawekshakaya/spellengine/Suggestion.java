package aksharawekshakaya.spellengine;

import java.io.Serializable;

public class Suggestion implements Serializable, Comparable<Suggestion>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String suggestion;
	private String word;
	private double score;
	
	public Suggestion(String word, String suggestion, double score) {
		this.word = word;
		this.suggestion = suggestion;
		this.score = score;
	}
	
	public double getScore() {
		return score;
	}
	
	protected void setScore(double score) {
		this.score = score;
	}

	public String getWord() {
		return word;
	}

	@Override
	public int compareTo(Suggestion o) { //compares only score
		if(o.getScore() > score) {
			return 1;
		} else if(o.getScore() < score) {
			return -1;
		} else {
			return 0;
		}
	}

	@Override
	public int hashCode() { //Differentiate two suggestions with same word
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(score);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) { //equal only if everything is same
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Suggestion other = (Suggestion) obj;
		if (Double.doubleToLongBits(score) != Double
				.doubleToLongBits(other.score))
			return false;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}

	public String getSuggestion() {
		return suggestion;
	}

	protected void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}
	
}
