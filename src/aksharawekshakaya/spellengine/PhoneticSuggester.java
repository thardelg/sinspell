package aksharawekshakaya.spellengine;

import java.util.List;
import java.util.Set;

import aksharawekshakaya.dictionaries.PhoneticDictionary;

public class PhoneticSuggester extends GenericSuggester {
	protected PhoneticDictionary dictionary;
	public PhoneticSuggester(PhoneticDictionary dictionary) {
		this.dictionary = dictionary;
	}
	@Override
	public List<Suggestion> suggest(String misspelled) {
		isSuggested = false;
		suggestions.clear();
		Set<String> suggs = dictionary.getSimilarSet(misspelled);
		if(suggs.isEmpty() || suggs==null) {
			return suggestions;
		} else {
			for(String s : suggs) {
				int score = 1; //FIXME add edit distance score
				suggestions.add(new Suggestion(misspelled, s, score));
			}
			isSuggested = true;
		}
		return suggestions;
		//FIXME remove <S>

	}

}
