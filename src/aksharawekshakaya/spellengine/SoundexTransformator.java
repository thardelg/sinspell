package aksharawekshakaya.spellengine;

import aksharawekshakaya.util.Normalizer;
import aksharawekshakaya.util.StringTokenizer;
import aksharawekshakaya.util.TokenType;
import aksharawekshakaya.util.TokenizerFactory;

public class SoundexTransformator extends PhoneticTransformator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4355202034730296967L;
	private final static String GRP_A = "\u0D85\u0D86\u0DCF";
	private final static String GRP_B = "\u0D89\u0D8A\u0DD2\u0DD3";
	private final static String GRP_C = "\u0D8B\u0D8C\u0DDF\u0DF3\u0DD4\u0DD6";
	private final static String GRP_D = "\u0D91\u0D92\u0DD9\u0DDA";
	private final static String GRP_E = "\u0D94\u0D95\u0DDC\u0DDD";
	private final static String GRP_F = "\u0D93\u0DDB";
	private final static String GRP_G = "\u0D96\u0DDE";
	private final static String GRP_H = "\u0D8D\u0D8E\u0DD8\u0DF2";
	private final static String GRP_I = "\u0D90\u0D8F";
	private final static String GRP_J = "\u0D9A\u0D9B\u0D9C\u0D9D\u0D9F";
	private final static String GRP_K = "\u0DA0\u0DA1\u0DA2\u0DA6\u0DA3\u0DA4\u0DA5";
	private final static String GRP_L = "\u0DAD\u0DAE\u0DAF\u0DB0\u0DB3";
	private final static String GRP_M = "\u0DB4\u0DB5\u0DB6\u0DB9\u0DC6\u0DB7";
	private final static String GRP_N = "\u0DA7\u0DA8\u0DA9\u0DAA\u0DAC";
	private final static String GRP_O = "\u0DBA";
	private final static String GRP_P = "\u0DC0";
	private final static String GRP_Q = "\u0DB1\u0DAB";
	private final static String GRP_R = "\u0DBD\u0DC5";
	private final static String GRP_S = "\u0DBB";
	private final static String GRP_T = "\u0DC3\u0DC1\u0DC2";
	private final static String GRP_U = "\u0D82\u0D83\u0D9E";
	private final static String GRP_V = "\u0D87\u0D88\u0DD0\u0DD1";
	private final static String GRP_W = "\u0DCA";
	private final static String GRP_X = "\u0DB8";
	private final static String GRP_Y = "\u0DC4";
	
	private final static String[] codeSet = {GRP_A, GRP_B, GRP_C, GRP_D, GRP_E, 
		GRP_F, GRP_G, GRP_H, GRP_I, GRP_J, GRP_K, GRP_L, GRP_M, GRP_N, GRP_O,
		GRP_P, GRP_Q, GRP_R, GRP_S, GRP_T, GRP_U, GRP_V, GRP_W, GRP_X, GRP_Y};
	
	private final static char[] replaceSet = {'0', '1', '2', '3', '4', '5', 
		'6', '7', '8', '9', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
		'u', 'v', 'w', 'x', 'y'};
	
	private Normalizer normalizer;
	private StringTokenizer tokenizer;
	private int span;
	
	public SoundexTransformator(String word, int span) {
		super(word);
		this.span = span;
	}

	@Override
	public String transform(final String word) {
		
		if((word == null) || (word.length() == 0)) {
			return " ";
		}
		
		normalizer = new Normalizer(word.trim());
		String source = normalizer.normalize();
		StringBuilder hash, mutableWord;
		hash = new StringBuilder();
		mutableWord = new StringBuilder(source);
		
		tokenizer = TokenizerFactory.getStringTokenizer(source, TokenType.SYLLABLE);
		String firstSyllable = tokenizer.next().value();
		if(firstSyllable == null) {
			return " ";
		}
		
		hash.append(firstSyllable);
		mutableWord.delete(0, firstSyllable.length());
		
		for(final char c : mutableWord.toString().toCharArray()) {
			int i = 0;
			for (String set : codeSet) {
				if(set.contains(String.valueOf(c))) {
					final char replace = replaceSet[i];
					final char prev = hash.charAt(hash.length()-1);
					if(prev == replace) {
						break;
					} else {
						hash.append(replaceSet[i]);
						break;
					}

				}
				i++;
			}
		}
		
		phoneticHash = refineHash(hash.toString());
		return  phoneticHash;
	}
	
	protected String refineHash(String code) {
		String refined = "";
		if(span == 0 ) {
			refined = code;
		} else {
			if (code.length() <= span) {
				refined = zeroPad(code);
			} else {
				refined = code.substring(0,span);
			}
		}
		return refined;
	}
	
	protected String zeroPad(String source) {
		StringBuilder sb = new StringBuilder(source);
		if(source.length() < span) {
			for(int i = 0; i < (span - source.length()); i++) {
				sb.append("0");
			}
		}
		return sb.toString();
	}

}
