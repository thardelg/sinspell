package aksharawekshakaya.spellengine;

public interface Transformable {
	public String transform(String word);
}
