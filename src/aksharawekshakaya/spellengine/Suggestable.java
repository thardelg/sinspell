package aksharawekshakaya.spellengine;

import java.util.List;

public interface Suggestable {
	public List<Suggestion> suggest(String word);
	public double getScore(Suggestion s);
	public double getMinScore();
	public double getMaxScore();
	public boolean isSuggested();
	public String getMispelled();
	public List<Suggestion> getCurrentList();
}
