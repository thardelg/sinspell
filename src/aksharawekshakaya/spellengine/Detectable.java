package aksharawekshakaya.spellengine;

public interface Detectable { //error detector interface
	public boolean detect(String word);
	public double getRate(); //return probability if not detected by lexicon
}
