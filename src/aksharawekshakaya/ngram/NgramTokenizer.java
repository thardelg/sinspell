package aksharawekshakaya.ngram;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import aksharawekshakaya.config.Configuration;
import aksharawekshakaya.config.PropertyConfig;
import aksharawekshakaya.util.StringTokenizer;
import aksharawekshakaya.util.TokenizerFactory;

public abstract class NgramTokenizer implements Iterator<String> , Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 281370071164151798L;
	protected String context;
	protected Set<String> tokenSet; //ensure same n-gram token is not inserted twice
	protected int gramSize;
	protected String edgeNgram; //represents the gram token at the beginning and end of a token
	protected int index; //current iteration position
	protected StringTokenizer tokenizer;
	protected PropertyConfig config;
	protected boolean isCalculated;
	protected boolean hasNext;
	protected NgramType type;
	
	
	protected NgramTokenizer(String context, NgramType gramType ) {
		this.type = gramType;
		this.context = context;
		this.gramSize = gramType.getGramSize();
		index = 0;
		config = PropertyConfig.getInstance();
		tokenSet = new LinkedHashSet<String>();
		edgeNgram = config.getProperty(Configuration.EDGE_NGRAM_TOKEN, "S");
		isCalculated = false;
		tokenizer = TokenizerFactory.getStringTokenizer(context, gramType.getTokenType());
		calculate();
		isCalculated = true;
		if(!tokenSet.isEmpty()) {
			hasNext = true;
		} else hasNext = false;
	}
	
	public abstract void calculate();

	public String getContext() {
		return context;
	}

	protected void setContext(String context) {
		this.context = context;
	}

	public int getGramSize() {
		return gramSize;
	}

	protected void setGramSize(int gramSize) {
		this.gramSize = gramSize;
	}

	public String getEdgeNgram() {
		return edgeNgram;
	}

	public void setEdgeNgram(String edgeNgram) {
		this.edgeNgram = edgeNgram;
	}

	public int getIndex() {
		return index;
	}

	protected void setIndex(int index) {
		this.index = index;
	}

	public StringTokenizer getTokenizer() {
		return tokenizer;
	}

	protected void setTokenizer(StringTokenizer tokenizer) {
		this.tokenizer = tokenizer;
	}

	public boolean isCalculated() {
		return isCalculated;
	}

	protected void setCalculated(boolean isCalculated) {
		this.isCalculated = isCalculated;
	}
	
	public Set<String> getTokenSet() {
		return tokenSet;
	}
	

}
