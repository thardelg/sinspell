package aksharawekshakaya.ngram;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import aksharawekshakaya.config.PropertyConfig;
import aksharawekshakaya.dictionaries.FileManager;
import aksharawekshakaya.dictionaries.KeyBasedDictionary;
import aksharawekshakaya.util.FileTokenizer;
import aksharawekshakaya.util.TokenType;
import aksharawekshakaya.util.TokenizerFactory;

public class FileNgramExtractor implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5252701693485061454L;
	protected File textFile;
	protected Map<String, Double> freqDistribution;
	protected Set<String> tokenSet;
	protected Map<String, Double> keyedTokenSet;
	protected StringNgramTokenizer ngramTokenizer;
	protected NgramType gramType;
	private boolean isReady;
	private Double totalCount;
	
	public FileNgramExtractor(File file, NgramType gramType, boolean isKeyBasedFile) {
		textFile = FileManager.getFile(file);
		tokenSet = new LinkedHashSet<String>();
		freqDistribution = new HashMap<String, Double>();
		keyedTokenSet = new HashMap<String, Double>();
		this.gramType = gramType;
		isReady = false;
		totalCount = null;
		if(isKeyBasedFile) {
			extractKeyBased(gramType.getTokenType());
		} else {
			extract(gramType.getTokenType());
		}
	}
	
	protected void extract(TokenType type) {
		FileTokenizer fileTokenizer = TokenizerFactory.getNFileTokenizer(textFile, type);
		while(fileTokenizer.hasNext()) {
			String s = fileTokenizer.next().value();
			tokenSet.add(s);
		}
		calculateFrequency();
	}

	protected void extractKeyBased(TokenType type) {
		KeyBasedDictionary dictionary = new KeyBasedDictionary(textFile);
		while(!dictionary.isReady());
		keyedTokenSet = dictionary.getCodeMap();
		calculateKeyedFrequency();
	}
	
	protected void calculateFrequency() {
		if(!tokenSet.isEmpty()) {
			for(String token : tokenSet) {
				ngramTokenizer = new StringNgramTokenizer(token, gramType);
				while(ngramTokenizer.hasNext())  {
					String gram = ngramTokenizer.next();
					if(freqDistribution.containsKey(gram)) {
						double freq = freqDistribution.get(gram);
						freqDistribution.put(gram, freq+1);
					} else freqDistribution.put(gram, (double)1);
				}
				ngramTokenizer = null;
			}
		}
		getTotal();
		tokenSet.clear();
		keyedTokenSet.clear();
		isReady = true;
	}
	
	protected void calculateKeyedFrequency() {
		if(!keyedTokenSet.isEmpty()) {
			for(Entry<String, Double> token : keyedTokenSet.entrySet()) {
				ngramTokenizer = new StringNgramTokenizer(token.getKey(), gramType);
				while(ngramTokenizer.hasNext())  {
					String gram = ngramTokenizer.next();
					if(freqDistribution.containsKey(gram)) {
						double freq = freqDistribution.get(gram);
						freqDistribution.put(gram, (freq + token.getValue()));
					} else freqDistribution.put(gram, token.getValue());
				}
				ngramTokenizer = null;
			}
		}
		getTotal();
		tokenSet.clear();
		keyedTokenSet.clear();
		isReady = true;
	}
	
	public double getFrequency(String gram) {
		return freqDistribution.get(gram);
	}
	public Map<String, Double> getFreqDistribution() {
		return freqDistribution;
	}
	public boolean isReady() {
		return isReady;
	}
	
	public Double getTotal() {
		if(totalCount == null) {
			totalCount = new Double(0);
			for(Double freq : freqDistribution.values()) {
				totalCount += freq;
			}
		}
		PropertyConfig ps = PropertyConfig.getInstance();
		ps.setProperty(gramType.getPropertyName(), totalCount);
		return totalCount;
	}
	
	public NgramType getGramType() {
		return gramType;
	}
}
