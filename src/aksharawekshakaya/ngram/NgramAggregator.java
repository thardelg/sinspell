package aksharawekshakaya.ngram;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import aksharawekshakaya.dictionaries.FileManager;

public class NgramAggregator {
	protected Map<String, Double> aggregatedDistribution;
	FileNgramExtractor fileExtractor;
	File corpusDirectory;
	protected NgramType gramType;
	private boolean isReady;
	private boolean isWriting;
	
	public NgramAggregator(File corpusDirectory, NgramType gramType, boolean isKeyBased) {
		this.gramType = gramType;
		this.corpusDirectory = corpusDirectory; 
		aggregatedDistribution =  new HashMap<String, Double>();
		isReady = false;
		isWriting = false;
		System.out.println("Setting up");
		aggregate(isKeyBased);
	}
	
	public void aggregate(boolean isKeyBased) {
		for(File file : corpusDirectory.listFiles()) {
			fileExtractor = new FileNgramExtractor(file, gramType, isKeyBased);
			while(!fileExtractor.isReady());
			for(Entry<String, Double> entry : fileExtractor.getFreqDistribution().entrySet()) {
				String key = entry.getKey();
				if(aggregatedDistribution.containsKey(key)) {
					double freq = aggregatedDistribution.get(entry.getKey());
					aggregatedDistribution.put(key, freq+1);
				} else aggregatedDistribution.put(key, (double)1);
			}
			fileExtractor = null;
		}
		isReady = true;
	}
	
	public Map<String, Double> getFreqDistribution() {
		return aggregatedDistribution;
	}
	public boolean isReady() {
		return isReady;
	}
	
	public void save(File file){
		while(!isReady());
		BufferedWriter out = FileManager.getWriter(file);
		StringBuilder sb = new StringBuilder();
		long totalNgrams = 0;
		for(Entry<String, Double> entry : aggregatedDistribution.entrySet()) {
			totalNgrams += entry.getValue();
			sb.append(entry.getKey()).append("\t").append(entry.getValue()).append("\n");
		}
		sb.append("Total").append("\t").append(totalNgrams).append("\n"); // sets Total
		
		try {
			isWriting = true;
			out.write(sb.toString());
			out.close();
			isWriting = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isWriting() {
		return isWriting;
	}
}
