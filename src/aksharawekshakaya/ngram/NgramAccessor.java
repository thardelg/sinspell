package aksharawekshakaya.ngram;

import aksharawekshakaya.config.PropertyConfig;
import aksharawekshakaya.dictionaries.IndexAccessor;

public class NgramAccessor {
	IndexAccessor inAccessor;
	NgramType type;
	PropertyConfig config;
	
	public NgramAccessor(String indexFilePath, String dataFilesPath, NgramType type) { //where index.idx is located
		inAccessor = new IndexAccessor(indexFilePath, dataFilesPath);
		this.type = type;
		config = PropertyConfig.getInstance();
	}
	
	public Long getFrequency(String gram) {
		return inAccessor.getValue(gram);
	}
	
	public long getTotal() {
		return config.getProperty(type.getPropertyName(), new Long(0));
	}
	
	public Double getProbability(String gram) {
		return new Double(getFrequency(gram) / getTotal());
	}
}
