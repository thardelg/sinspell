package aksharawekshakaya.ngram;

import java.io.File;

import aksharawekshakaya.dictionaries.DiskBasedDictionary;

public class NgramIndexer {
	private NgramAggregator aggregator;
	private DiskBasedDictionary diskDictionary;
	private String indexPath;
	private String dataPath;
	private String aggergatePath;
	private boolean isReady;

	public NgramIndexer(File corpusDirectory, NgramType gramType,
			String indexPath, String dataPath, String aggergatePath, boolean isKeyBased) {
		isReady = false;
		aggregator = new NgramAggregator(corpusDirectory, gramType, isKeyBased);
		while(!aggregator.isReady());
		this.indexPath = indexPath;
		this.dataPath = dataPath;
		indexNgrams(this.indexPath, this.dataPath);
	}
	
	protected void indexNgrams(String indexPath, String dataPath) {
		aggregator.save(new File(aggergatePath));
		while(aggregator.isWriting());
		diskDictionary = new DiskBasedDictionary(new File(aggergatePath), indexPath, dataPath, true);
		while(!diskDictionary.isReady());
		isReady = true;
	}
	
	public boolean isReady() {
		return isReady;
	}
}

