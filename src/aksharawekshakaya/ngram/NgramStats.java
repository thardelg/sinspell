package aksharawekshakaya.ngram;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import aksharawekshakaya.config.PropertyConfig;

public class NgramStats implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2818892413937184755L;
	private static final PropertyConfig ps = PropertyConfig.getInstance();
	private Map<String, Double> probDistribution;
	private NgramType gramType;
	private boolean isReady;
	
	public NgramStats(FileNgramExtractor upper, FileNgramExtractor lower) {
		this.gramType = lower.getGramType();
		isReady = false;
		generateDistribution(upper, lower);
		isReady = true;
	}
	
	public double getGramProb(double freq, NgramType type) { //type should be one lower
		return (freq / ps.getProperty(type.getPropertyName(), 0));
	}
	
	public double getBiGramProb(double bifreq, double unifreq) {
		return (bifreq/unifreq);
	}
	
	public void generateDistribution(FileNgramExtractor upper, FileNgramExtractor lower) {
		probDistribution = new HashMap<String, Double>();
		for(Entry<String, Double> gram : upper.getFreqDistribution().entrySet()) {
			
			String[] sets = gram.getKey().split(" ");
			String firstToken = "";
			if(gramType.getGramSize() == 1) {
				firstToken = sets[0];
			} else if(gramType.getGramSize() == 2) {
				firstToken = sets[0] + " " +sets[1];
			}
			
			if((firstToken == null) || (lower.getFreqDistribution().get(firstToken) == null)) {
				probDistribution.put(gram.getKey(), 0.0);
			} else {
				//System.out.println ("Key = " + gram.getKey() + "First = " + firstToken);
				double val = (double)gram.getValue()/(double)lower.getFreqDistribution().get(firstToken);
				probDistribution.put(gram.getKey(), val);
			}
			
		}
	}
	
	public double getProbability(String gram) {
		return probDistribution.get(gram);
	}
	
	public Map<String, Double> getDistribution() {
		return probDistribution;
	}
	
	public boolean isReady() {
		return isReady;
	}
}
