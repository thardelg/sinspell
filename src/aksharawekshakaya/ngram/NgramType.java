package aksharawekshakaya.ngram;

import aksharawekshakaya.util.TokenType;

public enum NgramType {
	WORD_UNI("WORD", 1 , TokenType.WORD),
	WORD_BI("WORD", 2, TokenType.WORD),
	WORD_TRI("WORD", 3, TokenType.WORD),
	WORD_QUAD("WORD", 4, TokenType.WORD),
	SYLLABLE_UNI("SYLLABLE", 1, TokenType.SYLLABLE),
	SYLLABLE_BI("SYLLABLE", 2, TokenType.SYLLABLE),
	SYLLABLE_TRI("SYLLABLE", 3, TokenType.SYLLABLE),
	SYLLABLE_QUAD("SYLLABLE", 4, TokenType.SYLLABLE);
	
	String baseType;
	int gramSize;
	TokenType type;
	
	NgramType(String baseType , int gramSize, TokenType type) {
		this.baseType = baseType;
		this.gramSize = gramSize;
		this.type = type;
	}

	public String getBaseType() {
		return baseType;
	}

	public void setBaseType(String baseType) {
		this.baseType = baseType;
	}

	public int getGramSize() {
		return gramSize;
	}

	public void setGramSize(int gramSize) {
		this.gramSize = gramSize;
	}

	public TokenType getTokenType() {
		return type;
	}

	public void setTokenType(TokenType type) {
		this.type = type;
	}
	
	public String getPropertyName() {
		String property = baseType;
		int n = gramSize;
		if(n == 1) 
			property += "_UNI";
		if(n == 2) 
			property += "_BI";
		if(n == 3) 
			property += "_TRI";
		if(n == 4) 
			property += "_QUAD";
		property += "GRAM_TOTAL";
		return property;
	}
	
	
}
