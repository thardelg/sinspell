package aksharawekshakaya.ngram;

import java.util.ArrayList;
import java.util.List;

public class StringNgramTokenizer extends NgramTokenizer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1723735908336070572L;

	public StringNgramTokenizer(String context, NgramType gramType) {
		super(context, gramType);
	}
	
	@Override
	public boolean hasNext() {
		return hasNext;
	}

	@Override
	public String next() {
		String token = "";
		if(hasNext) {
			List<String> s = new ArrayList<String>(tokenSet);
			token = s.get(index);
			index++;
			if(index == tokenSet.size()) {
				hasNext = false;
				index = 0;
			}
		}
		return token;
	}

	@Override
	public void remove() {
		List<String> s = new ArrayList<String>(tokenSet);
		if(index > 0) {
			s.remove(index-1);
		} else {
			s.remove(index);
		}
		
		tokenSet.clear();
		tokenSet.addAll(s);
	}

	@Override
	public void calculate() {
		List<String> tokens = new ArrayList<String>();
		//addLeftEdge(tokens);
		while(tokenizer.hasNext()) {
			tokens.add(tokenizer.next().value());
		}
        //addRightEdge(tokens);
        for (int i = 0; i < tokens.size() - gramSize + 1; i++)
            tokenSet.add(concat(tokens, i, i+ gramSize));
        
	}
	
	public static String concat(List<String> tokens, int start, int end) {
        StringBuilder sb = new StringBuilder();
        for (int i = start; i < end; i++)
            sb.append((i > start ? " " : "") + tokens.get(i));
        return sb.toString();
    }
	
	public void addLeftEdge(List<String> tokens) {
		for(int i=0; i<type.getGramSize()-1; i++) {
			tokens.add("<" + edgeNgram + ">");
		}
	}
	
	public void addRightEdge(List<String> tokens) {
		for(int i=0; i<type.getGramSize()-1; i++) {
			tokens.add("</" + edgeNgram + ">");
		}
	}

}
